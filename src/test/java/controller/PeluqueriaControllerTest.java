package controller;

import static org.junit.Assert.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.io.IOException;
import java.time.LocalTime;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;

import com.danielpid.hairstyle.ReactAndSpringDataRestApplication;
import com.danielpid.hairstyle.service.api.PeluqueriaDto;
import com.danielpid.hairstyle.service.api.PeluqueriaService;
import com.fasterxml.jackson.databind.ObjectMapper;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.MOCK, classes = ReactAndSpringDataRestApplication.class)
@AutoConfigureMockMvc
public class PeluqueriaControllerTest {

    @Autowired
    private MockMvc mvc;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private PeluqueriaService peluqueriaService;

    private PeluqueriaDto peluqueria;

    @Before
    public void init() {
	this.peluqueria = new PeluqueriaDto();
	this.peluqueria.setNombre("My test");
	this.peluqueria.setEmail("mytest_pelu_65169@mailinator.com");
	this.peluqueria.setIdMoneda((short) 1);
	this.peluqueria.setPasswordNueva("password");
	this.peluqueria.setHoraApertura(LocalTime.of(9, 30));
	this.peluqueria.setHoraCierre(LocalTime.of(21, 0));
    }

    @After
    public void clean() {
	this.peluqueriaService.delete(this.peluqueria.getEmail());
    }

    // ----- TESTS -----/

    @Test
    public void create() throws IOException, Exception {
	final ResultActions ra = this.mvc.perform(post("/api/peluqueria")
		.contentType(MediaType.APPLICATION_JSON)
		.content(JsonUtil.toJson(this.peluqueria)))
		.andExpect(status().isOk());
	final PeluqueriaDto dto = this.objectMapper.readValue(ra.andReturn().getResponse().getContentAsString(),
		PeluqueriaDto.class);
	assertEquals(this.peluqueria.getNombre(), dto.getNombre());
	assertEquals(this.peluqueria.getEmail(), dto.getEmail());
	assertEquals(this.peluqueria.getPasswordActual(), dto.getPasswordActual());
	assertEquals(this.peluqueria.getHoraApertura(), dto.getHoraApertura());
	assertEquals(this.peluqueria.getHoraCierre(), dto.getHoraCierre());
    }

}
