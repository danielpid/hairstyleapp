package com.danielpid.hairstyle.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.danielpid.hairstyle.service.api.CitaCriteriaDto;
import com.danielpid.hairstyle.service.api.CitaDto;
import com.danielpid.hairstyle.service.api.CitaService;

@RestController("citaController")
@RequestMapping("/api/citas")
public class CitaController {

    // ----- SERVICEs -----

    @Autowired
    private CitaService citaService;

    // ----- REST -----

    @PostMapping("/query")
    public List<CitaDto> findByCriteria(@RequestBody final CitaCriteriaDto criteria) {
	return this.citaService.findByCriteria(criteria);
    }

    @PostMapping
    public CitaDto create(@Valid @RequestBody final CitaDto dto) {
	return this.citaService.save(dto);
    }

    @PutMapping
    public CitaDto update(@Valid @RequestBody final CitaDto dto) {
	return this.citaService.update(dto);
    }

    @DeleteMapping("/{idCita}")
    public void delete(@PathVariable final Long idCita) {
	this.citaService.remove(idCita);
    }

}
