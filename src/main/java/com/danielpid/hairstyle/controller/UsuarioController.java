package com.danielpid.hairstyle.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.danielpid.hairstyle.service.api.ResetPasswordDto;
import com.danielpid.hairstyle.service.api.UsuarioService;
import com.danielpid.hairstyle.validation.InvalidUserException;

@Controller("usuarioController")
@RequestMapping("/api/usuario")
public class UsuarioController {

    // ----- SERVICEs -----

    @Autowired
    private UsuarioService usuarioService;

    // ----- REST -----

    @RequestMapping(value = "/email", method = RequestMethod.POST)
    public @ResponseBody boolean checkEmailAvailability(@RequestBody final String email) {
	return this.usuarioService.checkEmailAvailabilityPeluqueria(email);
    }

    @RequestMapping(value = "/resetPassword", method = RequestMethod.POST)
    public @ResponseBody String resetPassword(@Valid @RequestBody final ResetPasswordDto resetPasswordDto)
	    throws InvalidUserException {
	return this.usuarioService.resetPassword(resetPasswordDto);
    }

}
