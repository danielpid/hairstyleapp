package com.danielpid.hairstyle.controller;

import java.util.Locale;

import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Controller;

import com.danielpid.hairstyle.util.IdiomaEnum;

@Controller("baseController")
public class BaseController {

    /**
     * Recupera el idIdioma a partir del contexto
     * 
     * @return
     */
    protected Short getIdIdiomaFromContext() {
	final Locale locale = LocaleContextHolder.getLocale();
	if (locale != null
		&& locale.getLanguage() != null
		&& locale.getLanguage().toLowerCase().contains("en")) {
	    return IdiomaEnum.INGLES.getIdIdioma();
	}
	return IdiomaEnum.ESPANOL.getIdIdioma();
    }

}
