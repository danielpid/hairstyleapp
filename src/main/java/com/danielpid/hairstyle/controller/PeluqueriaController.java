package com.danielpid.hairstyle.controller;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.apache.commons.lang.BooleanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.danielpid.hairstyle.security.jwt.TokenAuthenticationService;
import com.danielpid.hairstyle.service.api.PeluqueriaDto;
import com.danielpid.hairstyle.service.api.PeluqueriaService;
import com.danielpid.hairstyle.validation.InvalidPasswordException;

@RestController("peluqueriaController")
@RequestMapping("/api/peluqueria")
public class PeluqueriaController {

    // ----- SERVICEs -----

    @Autowired
    private PeluqueriaService peluqueriaService;

    // ----- REST -----

    @GetMapping
    public @ResponseBody PeluqueriaDto findPeluqueria() {
	return this.peluqueriaService.findPeluqueria();
    }

    @PostMapping
    public @ResponseBody PeluqueriaDto create(@Valid @RequestBody final PeluqueriaDto dto) {
	return this.peluqueriaService.save(dto);
    }

    @PutMapping
    public @ResponseBody PeluqueriaDto update(@Valid @RequestBody final PeluqueriaDto dto,
	    final HttpServletResponse response)
	    throws InvalidPasswordException {
	final PeluqueriaDto dtoActualizado = this.peluqueriaService.update(dto);
	if (BooleanUtils.isTrue(dto.getEmailActualizado())) {
	    new TokenAuthenticationService().addAuthentication(response, dto.getEmail());
	}
	return dtoActualizado;
    }

}
