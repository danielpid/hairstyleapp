package com.danielpid.hairstyle.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.danielpid.hairstyle.service.api.ClienteCriteriaDto;
import com.danielpid.hairstyle.service.api.ClienteDto;
import com.danielpid.hairstyle.service.api.ClienteService;

@RestController("clienteController")
@RequestMapping("/api/clientes")
public class ClienteController {

    // ----- SERVICEs -----

    @Autowired
    private ClienteService clienteService;

    // ----- REST -----

    @GetMapping
    public List<ClienteDto> findAll() {
	return this.clienteService.findAll();
    }

    @PostMapping("/query")
    public List<ClienteDto> findByCriteria(@RequestBody final ClienteCriteriaDto criteria) {
	return this.clienteService.findByCriteria(criteria);
    }

    @PostMapping
    public ClienteDto create(@Valid @RequestBody final ClienteDto dto) {
	return this.clienteService.save(dto);
    }

    @PutMapping
    public ClienteDto update(@Valid @RequestBody final ClienteDto dto) {
	return this.clienteService.update(dto);
    }

    @PostMapping("/delete")
    @ResponseStatus(value = HttpStatus.NO_CONTENT)
    public void delete(@RequestBody final List<Integer> idsCliente) {
	this.clienteService.remove(idsCliente);
    }

}
