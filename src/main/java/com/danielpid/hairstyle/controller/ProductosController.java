package com.danielpid.hairstyle.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.danielpid.hairstyle.service.api.ProductoCriteriaDto;
import com.danielpid.hairstyle.service.api.ProductoDto;
import com.danielpid.hairstyle.service.api.ProductoService;

@RestController("productosController")
@RequestMapping("/api/productos")
public class ProductosController {

    // ----- SERVICEs -----

    @Autowired
    private ProductoService productoService;

    // ----- REST -----

    @GetMapping
    public List<ProductoDto> findAll() {
	return this.productoService.findAll();
    }

    @PostMapping("/query")
    public List<ProductoDto> findByCriteria(@RequestBody final ProductoCriteriaDto criteria) {
	return this.productoService.findByCriteria(criteria);
    }

    @PostMapping
    public ProductoDto create(@Valid @RequestBody final ProductoDto dto) {
	return this.productoService.save(dto);
    }

    @PutMapping
    public ProductoDto update(@Valid @RequestBody final ProductoDto dto) {
	return this.productoService.update(dto);
    }

    @PostMapping("/delete")
    @ResponseStatus(value = HttpStatus.NO_CONTENT)
    public void delete(@RequestBody final List<Integer> idsProducto) {
	this.productoService.remove(idsProducto);
    }

}
