package com.danielpid.hairstyle.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.danielpid.hairstyle.service.api.FacturaCriteriaDto;
import com.danielpid.hairstyle.service.api.FacturaDto;
import com.danielpid.hairstyle.service.api.FacturaService;

@RestController("facturaController")
@RequestMapping("/api/facturas")
public class FacturaController {

    // ----- SERVICEs -----

    @Autowired
    private FacturaService facturaService;

    // ----- REST -----

    @PostMapping("/query")
    public List<FacturaDto> findByCriteria(@RequestBody final FacturaCriteriaDto criteria) {
	return this.facturaService.findByCriteria(criteria);
    }

    @PostMapping
    public FacturaDto create(@Valid @RequestBody final FacturaDto dto) {
	return this.facturaService.save(dto);
    }

    @PutMapping
    public FacturaDto update(@Valid @RequestBody final FacturaDto dto) {
	return this.facturaService.update(dto);
    }

    @DeleteMapping("/{idFactura}")
    public void delete(@PathVariable final Long idFactura) {
	this.facturaService.removeFactura(idFactura);
    }

}
