package com.danielpid.hairstyle.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.danielpid.hairstyle.service.api.MonedaDto;
import com.danielpid.hairstyle.service.api.MonedaService;

@RestController("monedasController")
@RequestMapping("/api/monedas")
public class MonedasController {

    // ----- SERVICEs -----

    @Autowired
    private MonedaService monedaService;

    // ----- REST -----

    @GetMapping
    public List<MonedaDto> findAll() {
	return this.monedaService.findAll();
    }

}
