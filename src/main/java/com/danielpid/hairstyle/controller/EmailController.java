package com.danielpid.hairstyle.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.danielpid.hairstyle.service.api.UsuarioService;
import com.danielpid.hairstyle.validation.InvalidUserException;

@RestController("emailController")
@RequestMapping("/api/email")
public class EmailController {

    // ----- SERVICEs -----

    @Autowired
    private UsuarioService usuarioService;

    // ----- REST -----

    @PostMapping
    public boolean checkEmailAvailability(@RequestBody final String email) {
	return this.usuarioService.checkEmailAvailability(email);
    }

    @PostMapping("/forgot")
    public void forgotPassword(@RequestBody final String email) throws InvalidUserException {
	this.usuarioService.sendForgotEmail(email);
    }

}
