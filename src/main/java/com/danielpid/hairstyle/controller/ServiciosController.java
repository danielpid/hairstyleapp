package com.danielpid.hairstyle.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.danielpid.hairstyle.service.api.ServicioCriteriaDto;
import com.danielpid.hairstyle.service.api.ServicioDto;
import com.danielpid.hairstyle.service.api.ServicioService;

@RestController("serviciosController")
@RequestMapping("/api/servicios")
public class ServiciosController {

    // ----- SERVICEs -----

    @Autowired
    private ServicioService servicioService;

    // ----- REST -----

    @GetMapping
    public List<ServicioDto> findAll() {
	return this.servicioService.findAll();
    }

    @PostMapping("/query")
    public List<ServicioDto> findByCriteria(@RequestBody final ServicioCriteriaDto criteria) {
	return this.servicioService.findByCriteria(criteria);
    }

    @PostMapping
    public ServicioDto create(@Valid @RequestBody final ServicioDto dto) {
	return this.servicioService.save(dto);
    }

    @PutMapping
    public ServicioDto update(@Valid @RequestBody final ServicioDto dto) {
	return this.servicioService.update(dto);
    }

    @RequestMapping(value = "/delete", method = RequestMethod.POST)
    @ResponseStatus(value = HttpStatus.NO_CONTENT)
    public void delete(@RequestBody final List<Integer> idsServicio) {
	this.servicioService.remove(idsServicio);
    }

}
