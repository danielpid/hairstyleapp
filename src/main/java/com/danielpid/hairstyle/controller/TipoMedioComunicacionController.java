package com.danielpid.hairstyle.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.danielpid.hairstyle.service.api.TipoMedioComunicacionDto;
import com.danielpid.hairstyle.service.api.TipoMedioComunicacionService;

@RestController("tipoMedioComunicacionController")
@RequestMapping("/api/tiposMedioComunicacion")
public class TipoMedioComunicacionController extends BaseController {

    // ----- SERVICEs -----

    @Autowired
    private TipoMedioComunicacionService tipoMedioComunicacionService;

    // ----- REST -----

    @GetMapping("/{idTipoMedioComunicacionPadre}")
    public List<TipoMedioComunicacionDto> findByTipoMedioComunicacionPadre(
	    @PathVariable final Short idTipoMedioComunicacionPadre) {
	return this.tipoMedioComunicacionService.findByTipoMedioComunicacionPadre(this.getIdIdiomaFromContext(),
		idTipoMedioComunicacionPadre);
    }

}
