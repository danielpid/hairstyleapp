package com.danielpid.hairstyle.validation;

import org.springframework.security.access.AccessDeniedException;

import com.danielpid.hairstyle.entity.Usuario;
import com.danielpid.hairstyle.repository.api.UsuarioRepository;
import com.danielpid.hairstyle.service.api.BelongsToPeluqueria;
import com.danielpid.hairstyle.util.UsuarioUtils;

public class ValidationUtils {

    /**
     * Comprueba que el objeto que se quiere actualizar pertenezca a la peluquería para la que el usuario tiene permisos
     *
     * @param entity
     * @param usuarioRepository
     */
    public static void checkPeluqueria(final BelongsToPeluqueria entity,
	    final UsuarioRepository usuarioRepository) {
	final Usuario usuario = UsuarioUtils.getUsuarioFromContext(usuarioRepository);
	if (!entity.getIdPeluqueria().equals(usuario.getIdPeluqueria())) {
	    throw new AccessDeniedException("The producto does not match the current user's idPeluqueria");
	}
    }

}
