package com.danielpid.hairstyle.validation;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class RestExceptionHandler extends ResponseEntityExceptionHandler {

    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(
	    final MethodArgumentNotValidException ex,
	    final HttpHeaders headers, final HttpStatus status, final WebRequest request) {

	final BindingResult bindingResult = ex.getBindingResult();

	final List<FieldErrorDto> apiFieldErrors = bindingResult.getFieldErrors().stream()
		.map(fieldError -> new FieldErrorDto(fieldError.getField(), fieldError.getDefaultMessage(),
			fieldError.getRejectedValue()))
		.collect(Collectors.toList());

	final List<GlobalErrorDto> apiGlobalErrors = bindingResult.getGlobalErrors().stream()
		.map(globalError -> new GlobalErrorDto(globalError.getCode()))
		.collect(Collectors.toList());

	final ErrorsViewDto apiErrorsView = new ErrorsViewDto(apiFieldErrors, apiGlobalErrors);
	return new ResponseEntity<>(apiErrorsView, HttpStatus.UNPROCESSABLE_ENTITY);
    }

    @ExceptionHandler(value = InvalidPasswordException.class)
    protected ResponseEntity<Object> handleInvalidPassword(final Exception ex, final WebRequest request) {
	final String bodyOfResponse = "Invalid password";
	return this.handleExceptionInternal(ex, bodyOfResponse, new HttpHeaders(), HttpStatus.UNAUTHORIZED, request);
    }

    @ExceptionHandler(value = InvalidUserException.class)
    protected ResponseEntity<Object> handleInvalidUser(final Exception ex, final WebRequest request) {
	final String bodyOfResponse = "Invalid user";
	return this.handleExceptionInternal(ex, bodyOfResponse, new HttpHeaders(), HttpStatus.UNAUTHORIZED, request);
    }

}
