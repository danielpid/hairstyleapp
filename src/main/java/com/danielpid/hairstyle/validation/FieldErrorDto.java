package com.danielpid.hairstyle.validation;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;

@SuppressWarnings("serial")
@Data
@AllArgsConstructor
public class FieldErrorDto implements Serializable {

    private String field;
    private String code;
    private Object rejectedValue;

}
