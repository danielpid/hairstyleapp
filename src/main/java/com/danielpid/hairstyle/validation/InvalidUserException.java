package com.danielpid.hairstyle.validation;

@SuppressWarnings("serial")
public class InvalidUserException extends Exception {

    public InvalidUserException() {
	super("Invalid user");
    }

}
