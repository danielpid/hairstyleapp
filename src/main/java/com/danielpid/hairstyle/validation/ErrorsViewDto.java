package com.danielpid.hairstyle.validation;

import java.io.Serializable;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;

@SuppressWarnings("serial")
@Data
@AllArgsConstructor
public class ErrorsViewDto implements Serializable {

    private List<FieldErrorDto> fieldErrors;
    private List<GlobalErrorDto> globalErrors;

}
