package com.danielpid.hairstyle.validation;

@SuppressWarnings("serial")
public class InvalidPasswordException extends Exception {

    public InvalidPasswordException() {
	super("Invalid password");
    }

}
