package com.danielpid.hairstyle.validation;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;

@SuppressWarnings("serial")
@Data
@AllArgsConstructor
public class GlobalErrorDto implements Serializable {

    private String code;

}
