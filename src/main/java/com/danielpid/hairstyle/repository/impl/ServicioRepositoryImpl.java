package com.danielpid.hairstyle.repository.impl;

import java.util.List;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.hibernate.transform.Transformers;
import org.hibernate.type.FloatType;
import org.hibernate.type.IntegerType;
import org.hibernate.type.StringType;
import org.springframework.beans.factory.annotation.Autowired;

import com.danielpid.hairstyle.repository.api.ServicioRepositoryCustom;
import com.danielpid.hairstyle.service.api.ServicioCriteriaDto;
import com.danielpid.hairstyle.service.api.ServicioDto;

public class ServicioRepositoryImpl implements ServicioRepositoryCustom {

    @Autowired
    private SessionFactory sessionFactory;

    /**
     * @inheritDoc
     */
    @SuppressWarnings("unchecked")
    @Override
    public List<ServicioDto> findByCriteria(final ServicioCriteriaDto criteria) {
	final StringBuilder sb = new StringBuilder();
	sb.append("select id_servicio as idServicio, ");
	sb.append(" nombre as nombre, ");
	sb.append(" precio as precio ");
	sb.append("from servicio ");
	sb.append("where id_peluqueria=:idPeluqueria and borrado=false ");
	if (criteria != null) {
	    if (StringUtils.isNotBlank(criteria.getNombre())) {
		sb.append("and lower(nombre) like :nombre ");
	    }
	    if (criteria.getPrecio() != null) {
		sb.append("and precio=:precio ");
	    }
	    if (CollectionUtils.isNotEmpty(criteria.getIdsExcluir())) {
		sb.append("and id_servicio not in (:idsExcluir) ");
	    }
	}
	sb.append("order by nombre ");

	final Query query = this.sessionFactory.getCurrentSession().createSQLQuery(sb.toString())
		.addScalar("idServicio", IntegerType.INSTANCE)
		.addScalar("nombre", StringType.INSTANCE)
		.addScalar("precio", FloatType.INSTANCE)
		.setResultTransformer(Transformers.aliasToBean(ServicioDto.class))
		.setInteger("idPeluqueria", criteria.getIdPeluqueria());
	if (criteria != null) {
	    if (StringUtils.isNotBlank(criteria.getNombre())) {
		query.setString("nombre", "%" + criteria.getNombre().toLowerCase() + "%");
	    }
	    if (criteria.getPrecio() != null) {
		query.setFloat("precio", criteria.getPrecio());
	    }
	    if (CollectionUtils.isNotEmpty(criteria.getIdsExcluir())) {
		query.setParameterList("idsExcluir", criteria.getIdsExcluir());
	    }
	}
	return query.list();
    }

    /**
     * @inheritDoc
     */
    @Override
    public void deleteByIdServicioIn(final List<Integer> idsServicio) {
	final StringBuilder sb = new StringBuilder();
	sb.append("update servicio ");
	sb.append("set borrado=true ");
	sb.append("where id_servicio in (:idsServicio) ");
	final Query query = this.sessionFactory.getCurrentSession().createSQLQuery(sb.toString())
		.setParameterList("idsServicio", idsServicio);
	query.executeUpdate();
    }

}
