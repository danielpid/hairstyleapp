package com.danielpid.hairstyle.repository.impl;

import java.util.List;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.hibernate.transform.Transformers;
import org.hibernate.type.FloatType;
import org.hibernate.type.IntegerType;
import org.hibernate.type.StringType;
import org.springframework.beans.factory.annotation.Autowired;

import com.danielpid.hairstyle.repository.api.ProductoRepositoryCustom;
import com.danielpid.hairstyle.service.api.ProductoCriteriaDto;
import com.danielpid.hairstyle.service.api.ProductoDto;

public class ProductoRepositoryImpl implements ProductoRepositoryCustom {

    @Autowired
    private SessionFactory sessionFactory;

    /**
     * @inheritDoc
     */
    @SuppressWarnings("unchecked")
    @Override
    public List<ProductoDto> findByCriteria(final ProductoCriteriaDto criteria) {
	final StringBuilder sb = new StringBuilder();
	sb.append("select id_producto as idProducto, ");
	sb.append(" nombre as nombre, ");
	sb.append(" precio as precio, ");
	sb.append(" stock as stock ");
	sb.append("from producto ");
	sb.append("where id_peluqueria=:idPeluqueria and borrado=false ");
	if (criteria != null) {
	    if (CollectionUtils.isNotEmpty(criteria.getIdsProducto())) {
		sb.append("and id_producto in (:idsProducto) ");
	    }
	    if (StringUtils.isNotBlank(criteria.getNombre())) {
		sb.append("and lower(nombre) like :nombre ");
	    }
	    if (criteria.getPrecio() != null) {
		sb.append("and precio=:precio ");
	    }
	    if (criteria.getStock() != null) {
		sb.append("and stock=:stock ");
	    }
	    if (CollectionUtils.isNotEmpty(criteria.getIdsExcluir())) {
		sb.append("and id_producto not in (:idsExcluir) ");
	    }
	}
	sb.append("order by nombre ");

	final Query query = this.sessionFactory.getCurrentSession().createSQLQuery(sb.toString())
		.addScalar("idProducto", IntegerType.INSTANCE)
		.addScalar("nombre", StringType.INSTANCE)
		.addScalar("precio", FloatType.INSTANCE)
		.addScalar("stock", IntegerType.INSTANCE)
		.setResultTransformer(Transformers.aliasToBean(ProductoDto.class))
		.setInteger("idPeluqueria", criteria.getIdPeluqueria());
	if (criteria != null) {
	    if (CollectionUtils.isNotEmpty(criteria.getIdsProducto())) {
		query.setParameterList("idsProducto", criteria.getIdsProducto());
	    }
	    if (StringUtils.isNotBlank(criteria.getNombre())) {
		query.setString("nombre", "%" + criteria.getNombre().toLowerCase() + "%");
	    }
	    if (criteria.getPrecio() != null) {
		query.setFloat("precio", criteria.getPrecio());
	    }
	    if (criteria.getStock() != null) {
		query.setInteger("stock", criteria.getStock());
	    }
	    if (CollectionUtils.isNotEmpty(criteria.getIdsExcluir())) {
		query.setParameterList("idsExcluir", criteria.getIdsExcluir());
	    }
	}
	return query.list();
    }

    /**
     * @inheritDoc
     */
    @Override
    public void deleteByIdProductoIn(final List<Integer> idsProducto) {
	final StringBuilder sb = new StringBuilder();
	sb.append("update producto ");
	sb.append("set borrado=true ");
	sb.append("where id_producto in (:idsProducto) ");
	final Query query = this.sessionFactory.getCurrentSession().createSQLQuery(sb.toString())
		.setParameterList("idsProducto", idsProducto);
	query.executeUpdate();
    }

}
