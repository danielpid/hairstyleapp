package com.danielpid.hairstyle.repository.impl;

import java.util.List;

import org.hibernate.SessionFactory;
import org.hibernate.transform.Transformers;
import org.hibernate.type.IntegerType;
import org.hibernate.type.LongType;
import org.hibernate.type.StringType;
import org.hibernate.type.TimestampType;
import org.springframework.beans.factory.annotation.Autowired;

import com.danielpid.hairstyle.repository.api.CitaRepositoryCustom;
import com.danielpid.hairstyle.service.api.CitaCriteriaDto;
import com.danielpid.hairstyle.service.api.CitaDto;

public class CitaRepositoryImpl implements CitaRepositoryCustom {

    @Autowired
    private SessionFactory sessionFactory;

    /**
     * @inheritDoc
     */
    @SuppressWarnings("unchecked")
    @Override
    public List<CitaDto> findByCriteria(final CitaCriteriaDto criteria) {
	final StringBuilder sb = new StringBuilder();
	sb.append("select c.id_cita as idCita, cli.id_cliente as idCliente, cli.nombre as nombreCliente, ");
	sb.append("	c.fecha_desde as fechaDesde, c.fecha_hasta as fechaHasta, c.observaciones as observaciones ");
	sb.append("from cita c ");
	sb.append("inner join cliente cli on c.id_cliente=cli.id_cliente ");
	sb.append("	and cli.id_peluqueria=:idPeluqueria ");
	sb.append("where c.fecha_desde>=:fechaDesde and c.fecha_hasta<=:fechaHasta ");
	return this.sessionFactory.getCurrentSession().createSQLQuery(sb.toString())
		.addScalar("idCita", LongType.INSTANCE)
		.addScalar("idCliente", IntegerType.INSTANCE)
		.addScalar("nombreCliente", StringType.INSTANCE)
		.addScalar("fechaDesde", TimestampType.INSTANCE)
		.addScalar("fechaHasta", TimestampType.INSTANCE)
		.addScalar("observaciones", StringType.INSTANCE)
		.setResultTransformer(Transformers.aliasToBean(CitaDto.class))
		.setInteger("idPeluqueria", criteria.getIdPeluqueria())
		.setTimestamp("fechaDesde", criteria.getFechaDesde())
		.setTimestamp("fechaHasta", criteria.getFechaHasta())
		.list();
    }

}
