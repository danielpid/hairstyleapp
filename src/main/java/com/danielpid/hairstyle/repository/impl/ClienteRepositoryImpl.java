package com.danielpid.hairstyle.repository.impl;

import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.hibernate.transform.Transformers;
import org.hibernate.type.IntegerType;
import org.hibernate.type.StringType;
import org.springframework.beans.factory.annotation.Autowired;

import com.danielpid.hairstyle.repository.api.ClienteRepositoryCustom;
import com.danielpid.hairstyle.service.api.ClienteCriteriaDto;
import com.danielpid.hairstyle.service.api.ClienteDto;
import com.danielpid.hairstyle.util.TipoMedioComunicacionEnum;

public class ClienteRepositoryImpl implements ClienteRepositoryCustom {

    @Autowired
    private SessionFactory sessionFactory;

    /**
     * @inheritDoc
     */
    @SuppressWarnings("unchecked")
    @Override
    public List<ClienteDto> findAll(final Integer idPeluqueria) {
	final StringBuilder sb = new StringBuilder();
	sb.append("select c.id_cliente as idCliente, c.nombre as nombre, c.observaciones as observaciones, ");
	sb.append(" movil.id_medio_comunicacion as idMedioComunicacionMovil, ");
	sb.append(" movil.descripcion as descripcionMedioComunicacionMovil, ");
	sb.append(" casa.id_medio_comunicacion as idMedioComunicacionCasa, ");
	sb.append(" casa.descripcion as descripcionMedioComunicacionCasa, ");
	sb.append(" trabajo.id_medio_comunicacion as idMedioComunicacionTrabajo, ");
	sb.append(" trabajo.descripcion as descripcionMedioComunicacionTrabajo, ");
	sb.append(" email.id_medio_comunicacion as idMedioComunicacionEmail, ");
	sb.append(" email.descripcion as descripcionMedioComunicacionEmail ");
	sb.append("from cliente c ");
	sb.append("left join medio_comunicacion movil on c.id_cliente=movil.id_cliente ");
	sb.append(" and movil.id_tipo_medio_comunicacion=:idTipoMedioComunicacionMovil ");
	sb.append("left join medio_comunicacion casa on c.id_cliente=casa.id_cliente ");
	sb.append(" and casa.id_tipo_medio_comunicacion=:idTipoMedioComunicacionCasa ");
	sb.append("left join medio_comunicacion trabajo on c.id_cliente=trabajo.id_cliente ");
	sb.append(" and trabajo.id_tipo_medio_comunicacion=:idTipoMedioComunicacionTrabajo ");
	sb.append("left join medio_comunicacion email on c.id_cliente=email.id_cliente ");
	sb.append(" and email.id_tipo_medio_comunicacion=:idTipoMedioComunicacionEmail ");
	sb.append("where c.id_peluqueria=:idPeluqueria and borrado=:borrado ");
	return this.sessionFactory.getCurrentSession().createSQLQuery(sb.toString())
		.addScalar("idCliente", IntegerType.INSTANCE)
		.addScalar("nombre", StringType.INSTANCE)
		.addScalar("observaciones", StringType.INSTANCE)
		.addScalar("idMedioComunicacionMovil", IntegerType.INSTANCE)
		.addScalar("descripcionMedioComunicacionMovil", StringType.INSTANCE)
		.addScalar("idMedioComunicacionCasa", IntegerType.INSTANCE)
		.addScalar("descripcionMedioComunicacionCasa", StringType.INSTANCE)
		.addScalar("idMedioComunicacionTrabajo", IntegerType.INSTANCE)
		.addScalar("descripcionMedioComunicacionTrabajo", StringType.INSTANCE)
		.addScalar("idMedioComunicacionEmail", IntegerType.INSTANCE)
		.addScalar("descripcionMedioComunicacionEmail", StringType.INSTANCE)
		.setResultTransformer(Transformers.aliasToBean(ClienteDto.class))
		.setInteger("idPeluqueria", idPeluqueria)
		.setBoolean("borrado", false)
		.setShort("idTipoMedioComunicacionMovil", TipoMedioComunicacionEnum.MOVIL.getIdTipoMedioComunicacion())
		.setShort("idTipoMedioComunicacionCasa", TipoMedioComunicacionEnum.CASA.getIdTipoMedioComunicacion())
		.setShort("idTipoMedioComunicacionTrabajo",
			TipoMedioComunicacionEnum.TRABAJO.getIdTipoMedioComunicacion())
		.setShort("idTipoMedioComunicacionEmail", TipoMedioComunicacionEnum.EMAIL.getIdTipoMedioComunicacion())
		.list();
    }

    /**
     * @inheritDoc
     */
    @SuppressWarnings("unchecked")
    @Override
    public List<ClienteDto> findByCriteria(final ClienteCriteriaDto criteria) {
	final StringBuilder sb = new StringBuilder();
	sb.append("select c.id_cliente as idCliente, c.nombre as nombre, c.observaciones as observaciones, ");
	sb.append(" movil.id_medio_comunicacion as idMedioComunicacionMovil, ");
	sb.append(" movil.descripcion as descripcionMedioComunicacionMovil, ");
	sb.append(" casa.id_medio_comunicacion as idMedioComunicacionCasa, ");
	sb.append(" casa.descripcion as descripcionMedioComunicacionCasa, ");
	sb.append(" trabajo.id_medio_comunicacion as idMedioComunicacionTrabajo, ");
	sb.append(" trabajo.descripcion as descripcionMedioComunicacionTrabajo, ");
	sb.append(" email.id_medio_comunicacion as idMedioComunicacionEmail, ");
	sb.append(" email.descripcion as descripcionMedioComunicacionEmail ");
	sb.append("from cliente c ");
	sb.append("left join medio_comunicacion movil on c.id_cliente=movil.id_cliente ");
	sb.append(" and movil.id_tipo_medio_comunicacion=:idTipoMedioComunicacionMovil ");
	sb.append("left join medio_comunicacion casa on c.id_cliente=casa.id_cliente ");
	sb.append(" and casa.id_tipo_medio_comunicacion=:idTipoMedioComunicacionCasa ");
	sb.append("left join medio_comunicacion trabajo on c.id_cliente=trabajo.id_cliente ");
	sb.append(" and trabajo.id_tipo_medio_comunicacion=:idTipoMedioComunicacionTrabajo ");
	sb.append("left join medio_comunicacion email on c.id_cliente=email.id_cliente ");
	sb.append(" and email.id_tipo_medio_comunicacion=:idTipoMedioComunicacionEmail ");
	sb.append("where id_peluqueria=:idPeluqueria and borrado=:borrado ");
	if (criteria != null) {
	    if (StringUtils.isNotBlank(criteria.getNombre())) {
		sb.append("and lower(nombre) like :nombre ");
	    }
	    if (StringUtils.isNotBlank(criteria.getObservaciones())) {
		sb.append("and lower(observaciones) like :observaciones ");
	    }
	    if (StringUtils.isNotBlank(criteria.getDescripcionMedioComunicacion())) {
		sb.append("and (lower(movil.descripcion) like :medioComunicacion ");
		sb.append(" or lower(casa.descripcion) like :medioComunicacion ");
		sb.append(" or lower(trabajo.descripcion) like :medioComunicacion ");
		sb.append(" or lower(email.descripcion) like :medioComunicacion ) ");
	    }
	}
	sb.append("order by c.nombre ");

	final Query query = this.sessionFactory.getCurrentSession().createSQLQuery(sb.toString())
		.addScalar("idCliente", IntegerType.INSTANCE)
		.addScalar("nombre", StringType.INSTANCE)
		.addScalar("observaciones", StringType.INSTANCE)
		.addScalar("idMedioComunicacionMovil", IntegerType.INSTANCE)
		.addScalar("descripcionMedioComunicacionMovil", StringType.INSTANCE)
		.addScalar("idMedioComunicacionCasa", IntegerType.INSTANCE)
		.addScalar("descripcionMedioComunicacionCasa", StringType.INSTANCE)
		.addScalar("idMedioComunicacionTrabajo", IntegerType.INSTANCE)
		.addScalar("descripcionMedioComunicacionTrabajo", StringType.INSTANCE)
		.addScalar("idMedioComunicacionEmail", IntegerType.INSTANCE)
		.addScalar("descripcionMedioComunicacionEmail", StringType.INSTANCE)
		.setResultTransformer(Transformers.aliasToBean(ClienteDto.class))
		.setShort("idTipoMedioComunicacionMovil", TipoMedioComunicacionEnum.MOVIL.getIdTipoMedioComunicacion())
		.setShort("idTipoMedioComunicacionCasa", TipoMedioComunicacionEnum.CASA.getIdTipoMedioComunicacion())
		.setShort("idTipoMedioComunicacionTrabajo",
			TipoMedioComunicacionEnum.TRABAJO.getIdTipoMedioComunicacion())
		.setShort("idTipoMedioComunicacionEmail", TipoMedioComunicacionEnum.EMAIL.getIdTipoMedioComunicacion())
		.setInteger("idPeluqueria", criteria.getIdPeluqueria())
		.setBoolean("borrado", false);
	if (criteria != null) {
	    if (StringUtils.isNotBlank(criteria.getNombre())) {
		query.setString("nombre", "%" + criteria.getNombre() + "%");
	    }
	    if (StringUtils.isNotBlank(criteria.getObservaciones())) {
		query.setString("observaciones", "%" + criteria.getObservaciones() + "%");
	    }
	    if (StringUtils.isNotBlank(criteria.getDescripcionMedioComunicacion())) {
		query.setString("medioComunicacion", "%" + criteria.getDescripcionMedioComunicacion() + "%");
	    }
	}
	return query.list();
    }

}
