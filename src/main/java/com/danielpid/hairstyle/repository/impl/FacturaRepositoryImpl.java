package com.danielpid.hairstyle.repository.impl;

import java.util.List;

import org.hibernate.SessionFactory;
import org.hibernate.transform.Transformers;
import org.hibernate.type.FloatType;
import org.hibernate.type.IntegerType;
import org.hibernate.type.LongType;
import org.hibernate.type.ShortType;
import org.hibernate.type.StringType;
import org.hibernate.type.TimestampType;
import org.springframework.beans.factory.annotation.Autowired;

import com.danielpid.hairstyle.repository.api.FacturaRepositoryCustom;
import com.danielpid.hairstyle.service.api.FacturaCriteriaDto;
import com.danielpid.hairstyle.service.api.LineaFacturaDto;

public class FacturaRepositoryImpl implements FacturaRepositoryCustom {

    @Autowired
    private SessionFactory sessionFactory;

    /**
     * @inheritDoc
     */
    @SuppressWarnings("unchecked")
    @Override
    public List<LineaFacturaDto> findByCriteria(final FacturaCriteriaDto criteria) {
	final StringBuilder sb = new StringBuilder();
	sb.append("select f.id_factura as idFactura, c.id_cliente as idCliente, c.nombre as nombreCliente, ");
	sb.append("	f.fecha_alta as fechaAlta, f.precio as precioFactura, lf.id_linea_factura as idLineaFactura, ");
	sb.append("	p.id_producto as idProducto, p.nombre as nombreProducto, s.id_servicio as idServicio, ");
	sb.append("	s.nombre as nombreServicio, lf.cantidad as cantidad, ");
	sb.append("	COALESCE(s.precio, p.precio) as precioUnidad, lf.precio as precioLineaFactura ");
	sb.append("from factura f ");
	sb.append("inner join linea_factura lf on f.id_factura=lf.id_factura ");
	sb.append("inner join cliente c on f.id_cliente=c.id_cliente and c.id_peluqueria=:idPeluqueria ");
	sb.append("left join producto p on lf.id_producto=p.id_producto ");
	sb.append("left join servicio s on lf.id_servicio=s.id_servicio ");
	sb.append("where f.fecha_alta between :fechaDesde and :fechaHasta ");
	sb.append("order by f.id_factura ");
	return this.sessionFactory.getCurrentSession().createSQLQuery(sb.toString())
		.addScalar("idFactura", LongType.INSTANCE)
		.addScalar("idCliente", IntegerType.INSTANCE)
		.addScalar("nombreCliente", StringType.INSTANCE)
		.addScalar("fechaAlta", TimestampType.INSTANCE)
		.addScalar("precioFactura", FloatType.INSTANCE)
		.addScalar("idLineaFactura", LongType.INSTANCE)
		.addScalar("idProducto", IntegerType.INSTANCE)
		.addScalar("nombreProducto", StringType.INSTANCE)
		.addScalar("idServicio", IntegerType.INSTANCE)
		.addScalar("nombreServicio", StringType.INSTANCE)
		.addScalar("cantidad", ShortType.INSTANCE)
		.addScalar("precioUnidad", FloatType.INSTANCE)
		.addScalar("precioLineaFactura", FloatType.INSTANCE)
		.setResultTransformer(Transformers.aliasToBean(LineaFacturaDto.class))
		.setInteger("idPeluqueria", criteria.getIdPeluqueria())
		.setTimestamp("fechaDesde", criteria.getFechaDesde())
		.setTimestamp("fechaHasta", criteria.getFechaHasta())
		.list();
    }

}
