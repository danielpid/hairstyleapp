package com.danielpid.hairstyle.repository.api;

import java.util.List;

import org.springframework.data.repository.Repository;

import com.danielpid.hairstyle.entity.MedioComunicacion;

public interface MedioComunicacionRepository extends Repository<MedioComunicacion, Integer> {

    /**
     * Recupera el medio de comunicación por id
     *
     * @param idMedioComunicacion
     * @return
     */
    MedioComunicacion findOne(Integer idMedioComunicacion);

    /**
     * Recupera los medios de comunicación de los clientes dados
     * 
     * @param idsCliente
     * @return
     */
    List<MedioComunicacion> findByIdClienteIn(List<Integer> idsCliente);

    /**
     * Crea la entidad en bd
     *
     * @param medioComunicacion
     * @return
     */
    MedioComunicacion save(MedioComunicacion medioComunicacion);

    /**
     * Borra el medio de comunicación por id
     *
     * @param idMedioComunicacion
     */
    void deleteByIdMedioComunicacion(Integer idMedioComunicacion);

    /**
     * Borra los medios de comunicación por id
     *
     * @param idsMedioComunicacion
     */
    void deleteByIdMedioComunicacionIn(List<Integer> idsMedioComunicacion);

}
