package com.danielpid.hairstyle.repository.api;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.danielpid.hairstyle.entity.TipoMedioComunicacionIdioma;

public interface TipoMedioComunicacionRepository extends JpaRepository<TipoMedioComunicacionIdioma, Integer> {

    /**
     * Recupera los tipos de medio de comunicación para el padre dado
     *
     * @param idIdioma
     * @param idTipoMedioComunicacionPadre
     * @return
     */
    List<TipoMedioComunicacionIdioma> findByIdIdiomaAndTipoMedioComunicacionIdTipoMedioComunicacionPadre(Short idIdioma,
	    Short idTipoMedioComunicacionPadre);

}
