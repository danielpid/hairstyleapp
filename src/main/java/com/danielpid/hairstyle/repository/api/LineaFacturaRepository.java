package com.danielpid.hairstyle.repository.api;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.danielpid.hairstyle.entity.LineaFactura;

public interface LineaFacturaRepository extends JpaRepository<LineaFactura, Long> {

    /**
     * Recupera todas las líneas de factura para una factura
     *
     * @param idFactura
     * @return
     */
    List<LineaFactura> findByFacturaIdFacturaIn(Long idFactura);

}
