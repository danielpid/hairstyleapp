package com.danielpid.hairstyle.repository.api;

import org.springframework.data.repository.Repository;

import com.danielpid.hairstyle.entity.Peluqueria;

public interface PeluqueriaRepository extends Repository<Peluqueria, Integer> {

    /**
     * Recupera el peluqueria por id
     *
     * @param idPeluqueria
     * @return
     */
    Peluqueria findOne(Integer idPeluqueria);

    /**
     * Crea la entidad en bd
     *
     * @param peluqueria
     * @return
     */
    Peluqueria save(Peluqueria peluqueria);

    /**
     * Elimina la peluqueria dada
     * 
     * @param idPeluqueria
     */
    void deleteByIdPeluqueria(Integer idPeluqueria);

}
