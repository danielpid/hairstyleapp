package com.danielpid.hairstyle.repository.api;

import java.util.List;

import com.danielpid.hairstyle.service.api.FacturaCriteriaDto;
import com.danielpid.hairstyle.service.api.LineaFacturaDto;

public interface FacturaRepositoryCustom {

    /**
     * Recupera las facturas que cumplen con los criterios
     *
     * @param criteria
     * @return
     */
    List<LineaFacturaDto> findByCriteria(FacturaCriteriaDto criteria);

}
