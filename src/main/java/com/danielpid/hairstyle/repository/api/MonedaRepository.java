package com.danielpid.hairstyle.repository.api;

import org.springframework.data.jpa.repository.JpaRepository;

import com.danielpid.hairstyle.entity.Moneda;

public interface MonedaRepository extends JpaRepository<Moneda, Short> {

}
