package com.danielpid.hairstyle.repository.api;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.danielpid.hairstyle.entity.Cliente;

public interface ClienteRepository extends JpaRepository<Cliente, Integer>, ClienteRepositoryCustom {

    /**
     * Recupera los clientes por ids
     *
     * @param idsCliente
     * @return
     */
    List<Cliente> findByIdClienteIn(List<Integer> idsCliente);

    /**
     * Borra los clientes por id
     *
     * @param idsCliente
     */
    void deleteByIdClienteIn(List<Integer> idsCliente);

}
