package com.danielpid.hairstyle.repository.api;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.danielpid.hairstyle.entity.Producto;

public interface ProductoRepository extends JpaRepository<Producto, Integer>, ProductoRepositoryCustom {

    /**
     * Recupera todos los productos para la peluquería
     *
     * @return
     */
    List<Producto> findByIdPeluqueriaAndBorradoFalseOrderByNombre(Integer idPeluqueria);

}
