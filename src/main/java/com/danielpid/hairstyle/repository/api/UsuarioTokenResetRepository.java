package com.danielpid.hairstyle.repository.api;

import org.springframework.data.jpa.repository.JpaRepository;

import com.danielpid.hairstyle.entity.UsuarioTokenReset;

public interface UsuarioTokenResetRepository extends JpaRepository<UsuarioTokenReset, Integer> {

    /**
     * Recupera la información para resetear la password
     *
     * @param token
     * @return
     */
    UsuarioTokenReset findByToken(String token);

}
