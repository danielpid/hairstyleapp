package com.danielpid.hairstyle.repository.api;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.danielpid.hairstyle.entity.Servicio;

public interface ServicioRepository extends JpaRepository<Servicio, Integer>, ServicioRepositoryCustom {

    /**
     * Recupera todos los servicios para la peluquería
     *
     * @return
     */
    List<Servicio> findByIdPeluqueriaAndBorradoFalseOrderByNombre(Integer idPeluqueria);

}
