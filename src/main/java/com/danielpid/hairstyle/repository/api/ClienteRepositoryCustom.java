package com.danielpid.hairstyle.repository.api;

import java.util.List;

import com.danielpid.hairstyle.service.api.ClienteCriteriaDto;
import com.danielpid.hairstyle.service.api.ClienteDto;

public interface ClienteRepositoryCustom {

    /**
     * Recupera todos los clientes de la peluqueria
     *
     * @param idPeluqueria
     * @return
     */
    List<ClienteDto> findAll(Integer idPeluqueria);

    /**
     * Recupera todos los clientes que cumplen con los criterios
     * 
     * @param criteria
     * @return
     */
    List<ClienteDto> findByCriteria(ClienteCriteriaDto criteria);

}
