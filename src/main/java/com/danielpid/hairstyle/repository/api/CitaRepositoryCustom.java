package com.danielpid.hairstyle.repository.api;

import java.util.List;

import com.danielpid.hairstyle.service.api.CitaCriteriaDto;
import com.danielpid.hairstyle.service.api.CitaDto;

public interface CitaRepositoryCustom {

    /**
     * Recupera las citas que cumplen con los criterios dados
     * 
     * @param criteria
     * @return
     */
    List<CitaDto> findByCriteria(CitaCriteriaDto criteria);

}
