package com.danielpid.hairstyle.repository.api;

import org.springframework.data.jpa.repository.JpaRepository;

import com.danielpid.hairstyle.entity.Usuario;

public interface UsuarioRepository extends JpaRepository<Usuario, Integer> {

    /**
     * Recupera un usuario por el email
     *
     * @param email
     * @return
     */
    Usuario findByEmail(String email);

}