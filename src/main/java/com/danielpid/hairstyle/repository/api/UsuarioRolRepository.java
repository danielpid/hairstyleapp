package com.danielpid.hairstyle.repository.api;

import org.springframework.data.jpa.repository.JpaRepository;

import com.danielpid.hairstyle.entity.UsuarioRol;
import com.danielpid.hairstyle.entity.UsuarioRolId;

public interface UsuarioRolRepository extends JpaRepository<UsuarioRol, UsuarioRolId> {

    /**
     * Elimina todas las entidades UsuarioRol para el usuario dado
     *
     * @param idUsuario
     */
    void deleteByIdUsuario(Integer idUsuario);

}
