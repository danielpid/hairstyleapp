package com.danielpid.hairstyle.repository.api;

import java.util.List;

import com.danielpid.hairstyle.service.api.ServicioCriteriaDto;
import com.danielpid.hairstyle.service.api.ServicioDto;

public interface ServicioRepositoryCustom {

    /**
     * Recupera los servicios que cumplen con los criterios. Para los campos de texto usa fulltext search de MySQL
     *
     * @param criteria
     * @return
     */
    List<ServicioDto> findByCriteria(ServicioCriteriaDto criteria);

    /**
     * Borrado lógico de los servicios por id
     *
     * @param idsServicio
     */
    void deleteByIdServicioIn(List<Integer> idsServicio);

}
