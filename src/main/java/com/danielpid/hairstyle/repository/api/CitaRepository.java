package com.danielpid.hairstyle.repository.api;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.danielpid.hairstyle.entity.Cita;

public interface CitaRepository extends JpaRepository<Cita, Long>, CitaRepositoryCustom {

    /**
     * Recupera las citas para los clientes dados
     *
     * @param idsCliente
     * @return
     */
    List<Cita> findByClienteIdClienteIn(List<Integer> idsCliente);

}
