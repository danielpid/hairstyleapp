package com.danielpid.hairstyle.repository.api;

import org.springframework.data.jpa.repository.JpaRepository;

import com.danielpid.hairstyle.entity.Factura;

public interface FacturaRepository extends JpaRepository<Factura, Long>, FacturaRepositoryCustom {

}
