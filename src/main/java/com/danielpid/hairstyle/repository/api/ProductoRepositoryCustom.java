package com.danielpid.hairstyle.repository.api;

import java.util.List;

import com.danielpid.hairstyle.service.api.ProductoCriteriaDto;
import com.danielpid.hairstyle.service.api.ProductoDto;

public interface ProductoRepositoryCustom {

    /**
     * Recupera los productos que cumplen con los criterios. Para los campos de texto usa fulltext search de MySQL
     *
     * @param criteria
     * @return
     */
    List<ProductoDto> findByCriteria(ProductoCriteriaDto criteria);

    /**
     * Borrado lógico los productos por id
     *
     * @param idsProducto
     */
    void deleteByIdProductoIn(List<Integer> idsProducto);

}
