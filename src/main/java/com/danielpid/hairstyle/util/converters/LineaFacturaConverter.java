package com.danielpid.hairstyle.util.converters;

import com.danielpid.hairstyle.entity.Factura;
import com.danielpid.hairstyle.entity.LineaFactura;
import com.danielpid.hairstyle.entity.Producto;
import com.danielpid.hairstyle.entity.Servicio;
import com.danielpid.hairstyle.service.api.LineaFacturaDto;

public class LineaFacturaConverter {

    /**
     * Crea un entity LineaFactura
     *
     * @param lf
     * @param factura
     * @return
     */
    public static LineaFactura toEntity(final LineaFacturaDto lf, final Factura factura) {
	final Producto producto = lf.getIdProducto() != null ? new Producto(lf.getIdProducto()) : null;
	final Servicio servicio = lf.getIdServicio() != null ? new Servicio(lf.getIdServicio()) : null;
	return new LineaFactura(factura, producto, servicio, lf.getPrecioLineaFactura(), lf.getCantidad());
    }

    /**
     * Actualiza la entity con la información del dto
     *
     * @param entity
     * @param dto
     * @return
     */
    public static LineaFactura updateEntity(final LineaFactura entity, final LineaFacturaDto dto) {
	entity.setCantidad(dto.getCantidad());
	entity.setPrecio(dto.getPrecioLineaFactura());
	entity.setProducto(dto.getIdProducto() != null ? new Producto(dto.getIdProducto()) : null);
	entity.setServicio(dto.getIdServicio() != null ? new Servicio(dto.getIdServicio()) : null);
	return entity;
    }

}
