package com.danielpid.hairstyle.util.converters;

import com.danielpid.hairstyle.entity.Moneda;
import com.danielpid.hairstyle.entity.Peluqueria;
import com.danielpid.hairstyle.service.api.PeluqueriaDto;

public class PeluqueriaConverter {

    /**
     * Updates the values of the entity with the dto values
     *
     * @param dto
     * @param entity
     * @param moneda
     */
    public static void updateEntity(final PeluqueriaDto dto, final Peluqueria entity, final Moneda moneda) {
	entity.setNombre(dto.getNombre());
	entity.setHoraApertura(dto.getHoraApertura());
	entity.setHoraCierre(dto.getHoraCierre());
	entity.setMoneda(moneda);
	entity.setMostrarTotalDiario(dto.getMostrarTotalDiario() != null ? dto.getMostrarTotalDiario() : true);
    }

    /**
     * Creates a new entity with the dto values
     *
     * @param dto
     * @param moneda
     * @return
     */
    public static Peluqueria toEntity(final PeluqueriaDto dto, final Moneda moneda) {
	final Peluqueria entity = new Peluqueria();
	updateEntity(dto, entity, moneda);
	return entity;
    }

}
