package com.danielpid.hairstyle.util;

public enum IdiomaEnum {

    ESPANOL((short) 1), INGLES((short) 2);

    private Short idIdioma;

    private IdiomaEnum(final Short idIdioma) {
	this.idIdioma = idIdioma;
    }

    public Short getIdIdioma() {
	return this.idIdioma;
    }

}
