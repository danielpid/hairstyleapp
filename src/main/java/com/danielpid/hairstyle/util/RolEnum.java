package com.danielpid.hairstyle.util;

public enum RolEnum {

    PROPIETARIO((short) 1);

    private Short idRol;

    private RolEnum(final Short idRol) {
	this.idRol = idRol;
    }

    public Short getIdRol() {
	return this.idRol;
    }

}
