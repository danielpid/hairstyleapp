package com.danielpid.hairstyle.util;

public enum TipoMedioComunicacionEnum {

    MOVIL((short) 2), CASA((short) 3), TRABAJO((short) 4), EMAIL((short) 5);

    private Short idTipoMedioComunicacion;

    private TipoMedioComunicacionEnum(final Short idTipoMedioComunicacion) {
	this.idTipoMedioComunicacion = idTipoMedioComunicacion;
    }

    public Short getIdTipoMedioComunicacion() {
	return this.idTipoMedioComunicacion;
    }

}
