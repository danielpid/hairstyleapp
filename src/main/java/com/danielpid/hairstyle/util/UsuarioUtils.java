package com.danielpid.hairstyle.util;

import org.springframework.security.core.context.SecurityContextHolder;

import com.danielpid.hairstyle.entity.Usuario;
import com.danielpid.hairstyle.repository.api.UsuarioRepository;

public class UsuarioUtils {

    /**
     * Recupera la entity {@link Usuario} a partir del nombre del usuario autenticado y el {@link UsuarioRepository}
     * pasado por parámetro
     * 
     * @param usuarioRepository
     * @return
     */
    public static Usuario getUsuarioFromContext(final UsuarioRepository usuarioRepository) {
	final String email = SecurityContextHolder.getContext().getAuthentication().getName();
	return usuarioRepository.findByEmail(email);
    }

}
