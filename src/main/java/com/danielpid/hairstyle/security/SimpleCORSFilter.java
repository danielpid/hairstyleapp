package com.danielpid.hairstyle.security;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SimpleCORSFilter implements Filter {

    private final Logger log = LoggerFactory.getLogger(SimpleCORSFilter.class);

    private final String urlWebappClient;

    public SimpleCORSFilter(final String urlWebappClient) {
	this.urlWebappClient = urlWebappClient;
	this.log.info("SimpleCORSFilter init");
    }

    @Override
    public void doFilter(final ServletRequest req, final ServletResponse res, final FilterChain chain)
	    throws IOException, ServletException {

	final HttpServletRequest request = (HttpServletRequest) req;
	final HttpServletResponse response = (HttpServletResponse) res;
	response.setHeader("Access-Control-Allow-Origin", this.urlWebappClient);
	response.setHeader("Access-Control-Expose-Headers", "Authorization");
	// response.setHeader("Access-Control-Allow-Credentials", "true");
	response.setHeader("Access-Control-Allow-Methods", "POST, GET, OPTIONS, DELETE, PUT");
	response.setHeader("Access-Control-Max-Age", "3600");
	response.setHeader("Access-Control-Allow-Headers", "Content-Type, Accept, Authorization");
	if ("OPTIONS".equalsIgnoreCase(request.getMethod())) {
	    response.setStatus(HttpServletResponse.SC_OK);
	} else {
	    chain.doFilter(req, res);
	}
    }

    @Override
    public void init(final FilterConfig filterConfig) {
    }

    @Override
    public void destroy() {
    }

}
