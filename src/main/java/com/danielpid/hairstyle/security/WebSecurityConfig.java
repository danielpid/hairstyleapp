package com.danielpid.hairstyle.security;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import com.danielpid.hairstyle.security.jwt.JWTAuthenticationFilter;
import com.danielpid.hairstyle.security.jwt.JWTLoginFilter;

@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    @Value("${url.webapp.client}")
    private String urlWebappClient;

    @Autowired
    private DataSource datasource;

    @Bean
    public PasswordEncoder passwordEncoder() {
	return new BCryptPasswordEncoder();
    }

    @Override
    protected void configure(final HttpSecurity http) throws Exception {
	// disable caching
	http.headers().cacheControl();
	http.csrf().disable() // disable csrf for our requests.
		.authorizeRequests()
		.antMatchers(HttpMethod.POST, "/login").permitAll()
		.antMatchers(HttpMethod.POST, "/api/peluqueria").permitAll()
		.antMatchers(HttpMethod.POST, "/api/email").permitAll()
		.antMatchers(HttpMethod.POST, "/api/email/forgot").permitAll()
		.antMatchers(HttpMethod.POST, "/api/usuario/resetPassword").permitAll()
		.antMatchers(HttpMethod.GET, "/api/monedas").permitAll()
		.anyRequest().authenticated()
		.and()
		// We filter the api/login requests
		.addFilterBefore(new JWTLoginFilter("/login", this.authenticationManager()),
			UsernamePasswordAuthenticationFilter.class)
		// And filter other requests to check the presence of JWT in header
		.addFilterBefore(new JWTAuthenticationFilter(), UsernamePasswordAuthenticationFilter.class)
		.addFilterBefore(new SimpleCORSFilter(this.urlWebappClient), JWTLoginFilter.class);
    }

    @Override
    protected void configure(final AuthenticationManagerBuilder auth) throws Exception {
	final StringBuilder sb = new StringBuilder();
	sb.append("select u.email,r.nombre ");
	sb.append("from usuario_rol ur ");
	sb.append("inner join rol r on ur.id_rol=r.id_rol ");
	sb.append("inner join usuario u on ur.id_usuario=u.id_usuario ");
	sb.append("where u.email=? ");
	auth.jdbcAuthentication().dataSource(this.datasource)
		.usersByUsernameQuery("select email,password,enabled from usuario where email=?")
		.authoritiesByUsernameQuery(sb.toString())
		.passwordEncoder(this.passwordEncoder());
    }

}
