package com.danielpid.hairstyle.security.jwt;

import java.util.Collection;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;

@SuppressWarnings("serial")
public class AuthenticatedUser implements Authentication {

    private final String name;
    private boolean authenticated = true;

    AuthenticatedUser(final String name) {
	this.name = name;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
	return null;
    }

    @Override
    public Object getCredentials() {
	return null;
    }

    @Override
    public Object getDetails() {
	return null;
    }

    @Override
    public Object getPrincipal() {
	return null;
    }

    @Override
    public boolean isAuthenticated() {
	return this.authenticated;
    }

    @Override
    public void setAuthenticated(final boolean b) throws IllegalArgumentException {
	this.authenticated = b;
    }

    @Override
    public String getName() {
	return this.name;
    }
}
