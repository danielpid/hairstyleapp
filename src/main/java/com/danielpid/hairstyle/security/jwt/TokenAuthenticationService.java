package com.danielpid.hairstyle.security.jwt;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.core.Authentication;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

public class TokenAuthenticationService {

    // private final long EXPIRATIONTIME = 1000 * 60 * 60 * 24 * 10; // 10 days
    private final String secret = "P3p1T0_-_Gr1ll0";
    private final String tokenPrefix = "Bearer";
    private final String headerString = "Authorization";

    public void addAuthentication(final HttpServletResponse response, final String username) {
	// We generate a token now.
	final String JWT = Jwts.builder()
		.setSubject(username)
		// .setExpiration(new Date(System.currentTimeMillis() + this.EXPIRATIONTIME))
		.signWith(SignatureAlgorithm.HS512, this.secret)
		.compact();
	response.addHeader(this.headerString, this.tokenPrefix + " " + JWT);
    }

    public Authentication getAuthentication(final HttpServletRequest request) {
	try {
	    final String token = request.getHeader(this.headerString);
	    if (token != null) {
		// parse the token.
		final String username = Jwts.parser()
			.setSigningKey(this.secret)
			.parseClaimsJws(token)
			.getBody()
			.getSubject();
		if (username != null) {
		    // we managed to retrieve a user
		    return new AuthenticatedUser(username);
		}
	    }
	} catch (final Exception e) {
	    // it will return null -> http:403
	}
	return null;
    }
}
