package com.danielpid.hairstyle.manager.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.danielpid.hairstyle.entity.Factura;
import com.danielpid.hairstyle.entity.LineaFactura;
import com.danielpid.hairstyle.entity.Producto;
import com.danielpid.hairstyle.manager.api.LineaFacturaManager;
import com.danielpid.hairstyle.repository.api.LineaFacturaRepository;
import com.danielpid.hairstyle.repository.api.ProductoRepository;
import com.danielpid.hairstyle.service.api.FacturaDto;
import com.danielpid.hairstyle.service.api.LineaFacturaDto;
import com.danielpid.hairstyle.util.converters.LineaFacturaConverter;

@Component("lineaFacturaManager")
public class LineaFacturaManagerImpl implements LineaFacturaManager {

    // ----- REPOs -----

    @Autowired
    private LineaFacturaRepository lineaFacturaRepository;
    @Autowired
    private ProductoRepository productoRepository;

    // ----- IMPLEMENTATION -----

    /**
     * @inheritDoc
     */
    @Override
    public Long saveLineaFactura(final LineaFacturaDto lfDto, final Factura factura) {
	final LineaFactura saved = this.lineaFacturaRepository.save(LineaFacturaConverter.toEntity(lfDto, factura));
	this.updateStockProducto(saved.getProducto(), Integer.valueOf(lfDto.getCantidad()));
	return saved.getIdLineaFactura();
    }

    /**
     * @inheritDoc
     */
    @Override
    public void updateLineaFactura(final FacturaDto dto, final LineaFactura lfEntity) {
	final LineaFacturaDto lfDto = dto.getDetalle().stream()
		.filter(el -> el.getIdLineaFactura().equals(lfEntity.getIdLineaFactura()))
		.findFirst().get();
	// update the stock according to the change on the quantities
	final Integer cantidad = lfDto.getCantidad() - lfEntity.getCantidad();
	this.lineaFacturaRepository.save(LineaFacturaConverter.updateEntity(lfEntity, lfDto));
	this.updateStockProducto(lfEntity.getProducto(), cantidad);
    }

    /**
     * @inheritDoc
     */
    @Override
    public void deleteLineaFactura(final LineaFactura lfEntity) {
	this.lineaFacturaRepository.delete(lfEntity.getIdLineaFactura());
	this.updateStockProducto(lfEntity.getProducto(), -lfEntity.getCantidad());
    }

    /**
     * Actualiza la línea del stock para el producto
     *
     * @param producto
     * @param cantidad
     * @param lfDto
     */
    private void updateStockProducto(final Producto producto, final Integer cantidad) {
	if (producto != null) {
	    final Producto entity = this.productoRepository.findOne(producto.getIdProducto());
	    if (entity.getStock() != null) {
		final Integer stockUpdated = entity.getStock() - cantidad;
		if (stockUpdated < 0) {
		    entity.setStock(0);
		} else {
		    entity.setStock(stockUpdated);
		}
	    }
	}
    }

}
