package com.danielpid.hairstyle.manager.impl;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.danielpid.hairstyle.entity.Cliente;
import com.danielpid.hairstyle.manager.api.ClienteManager;
import com.danielpid.hairstyle.repository.api.ClienteRepository;
import com.danielpid.hairstyle.repository.api.UsuarioRepository;
import com.danielpid.hairstyle.service.api.ClienteDto;
import com.danielpid.hairstyle.util.UsuarioUtils;

@Component("clienteManager")
public class ClienteManagerImpl implements ClienteManager {

    // ----- REPOs -----

    @Autowired
    private ClienteRepository clienteRepository;
    @Autowired
    private UsuarioRepository usuarioRepository;

    // ----- IMPLEMENTATION -----

    /**
     * @inheritDoc
     */
    @Override
    public Cliente save(final ClienteDto dto) {
	final Cliente entity = this.clienteRepository.save(
		new Cliente(StringUtils.trimToNull(dto.getNombre()), StringUtils.trimToNull(dto.getObservaciones())));
	entity.setIdPeluqueria(UsuarioUtils.getUsuarioFromContext(this.usuarioRepository).getIdPeluqueria());
	return entity;
    }

}
