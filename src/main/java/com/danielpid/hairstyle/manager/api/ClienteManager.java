package com.danielpid.hairstyle.manager.api;

import com.danielpid.hairstyle.entity.Cliente;
import com.danielpid.hairstyle.service.api.ClienteDto;

public interface ClienteManager {

    /**
     * Crea un nuevo cliente en base de datos
     *
     * @param dto
     * @return
     */
    Cliente save(ClienteDto dto);

}
