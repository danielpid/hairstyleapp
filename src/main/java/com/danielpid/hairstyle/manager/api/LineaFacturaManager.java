package com.danielpid.hairstyle.manager.api;

import com.danielpid.hairstyle.entity.Factura;
import com.danielpid.hairstyle.entity.LineaFactura;
import com.danielpid.hairstyle.service.api.FacturaDto;
import com.danielpid.hairstyle.service.api.LineaFacturaDto;

public interface LineaFacturaManager {

    /**
     * Guarda la línea de factura y actualiza el stock
     *
     * @param lf
     * @param factura
     * @return
     */
    Long saveLineaFactura(final LineaFacturaDto lf, final Factura factura);

    /**
     * Actualiza la línea de factura con los nuevos valores
     *
     * @param dto
     * @param lfEntity
     */
    void updateLineaFactura(final FacturaDto dto, final LineaFactura lfEntity);

    /**
     * Elimina la línea de factura dada
     * 
     * @param lfEntity
     */
    void deleteLineaFactura(final LineaFactura lfEntity);

}
