package com.danielpid.hairstyle.service.impl;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.danielpid.hairstyle.repository.api.MonedaRepository;
import com.danielpid.hairstyle.service.api.MonedaDto;
import com.danielpid.hairstyle.service.api.MonedaService;

@Service("monedaService")
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
public class MonedaServiceImpl implements MonedaService {

    // ----- REPOs -----

    @Autowired
    private MonedaRepository monedaRepository;

    /**
     * {@inheritDoc}
     */
    @Override
    public List<MonedaDto> findAll() {
	return this.monedaRepository.findAll().stream()
		.map(entity -> new MonedaDto(entity.getIdMoneda(), entity.getSimbolo(), entity.getNombre()))
		.collect(Collectors.toList());
    }

}
