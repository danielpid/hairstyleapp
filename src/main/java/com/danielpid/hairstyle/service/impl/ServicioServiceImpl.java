package com.danielpid.hairstyle.service.impl;

import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.danielpid.hairstyle.entity.Servicio;
import com.danielpid.hairstyle.entity.Usuario;
import com.danielpid.hairstyle.repository.api.ServicioRepository;
import com.danielpid.hairstyle.repository.api.UsuarioRepository;
import com.danielpid.hairstyle.service.api.ServicioCriteriaDto;
import com.danielpid.hairstyle.service.api.ServicioDto;
import com.danielpid.hairstyle.service.api.ServicioService;
import com.danielpid.hairstyle.util.UsuarioUtils;
import com.danielpid.hairstyle.validation.ValidationUtils;

@Service("servicioService")
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
public class ServicioServiceImpl implements ServicioService {

    // ----- REPOs -----

    @Autowired
    private ServicioRepository servicioRepository;
    @Autowired
    private UsuarioRepository usuarioRepository;

    // ----- IMPLEMENTATION -----

    /**
     * @inheritDoc
     */
    @Override
    public List<ServicioDto> findAll() {
	final Usuario usuario = UsuarioUtils.getUsuarioFromContext(this.usuarioRepository);
	final List<Servicio> entities = this.servicioRepository
		.findByIdPeluqueriaAndBorradoFalseOrderByNombre(usuario.getIdPeluqueria());
	return entities.stream().map(servicio -> new ServicioDto(servicio.getIdServicio(), servicio.getNombre(),
		servicio.getPrecio())).collect(Collectors.toList());
    }

    /**
     * @inheritDoc
     */
    @Override
    public List<ServicioDto> findByCriteria(final ServicioCriteriaDto criteria) {
	final Usuario usuario = UsuarioUtils.getUsuarioFromContext(this.usuarioRepository);
	criteria.setIdPeluqueria(usuario.getIdPeluqueria());
	return this.servicioRepository.findByCriteria(criteria);
    }

    /**
     * @inheritDoc
     */
    @Override
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    public ServicioDto save(final ServicioDto dto) {
	final Servicio entity = this.servicioRepository
		.save(new Servicio(StringUtils.trimToNull(dto.getNombre()), dto.getPrecio()));
	dto.setIdServicio(entity.getIdServicio());
	return dto;
    }

    /**
     * @inheritDoc
     */
    @Override
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    public ServicioDto update(final ServicioDto dto) {
	final Servicio entity = this.servicioRepository.findOne(dto.getIdServicio());
	ValidationUtils.checkPeluqueria(entity, this.usuarioRepository);
	entity.setNombre(StringUtils.trimToNull(dto.getNombre()));
	entity.setPrecio(dto.getPrecio());
	this.servicioRepository.save(entity);
	return dto;
    }

    /**
     * @inheritDoc
     */
    @Override
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    public void remove(final List<Integer> idsServicio) {
	this.servicioRepository.deleteByIdServicioIn(idsServicio);
    }

}
