package com.danielpid.hairstyle.service.impl;

import java.util.Date;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.danielpid.hairstyle.entity.Usuario;
import com.danielpid.hairstyle.entity.UsuarioTokenReset;
import com.danielpid.hairstyle.repository.api.UsuarioRepository;
import com.danielpid.hairstyle.repository.api.UsuarioTokenResetRepository;
import com.danielpid.hairstyle.service.api.EmailService;
import com.danielpid.hairstyle.service.api.ResetPasswordDto;
import com.danielpid.hairstyle.service.api.UsuarioService;
import com.danielpid.hairstyle.util.Messages;
import com.danielpid.hairstyle.util.UsuarioUtils;
import com.danielpid.hairstyle.validation.InvalidUserException;

@Service("usuarioService")
public class UsuarioServiceImpl implements UsuarioService {

    // 1 hour
    private static int TOKEN_DURATION_MS = 1000 * 60 * 60;

    @Autowired
    private Messages messages;
    @Autowired
    private PasswordEncoder passwordEncoder;
    @Value("${url.webapp.client}")
    private String urlWebappClient;

    // ----- EMAIL -----

    @Autowired
    public SimpleMailMessage template;
    @Autowired
    public EmailService emailService;

    // ---- REPOs -----

    @Autowired
    private UsuarioRepository usuarioRepository;
    @Autowired
    private UsuarioTokenResetRepository usuarioTokenResetRepository;

    // ----- IMPLEMENTATION -----

    /**
     * @inheritDoc
     */
    @Override
    public boolean checkEmailAvailability(final String email) {
	final Usuario usuario = this.usuarioRepository.findByEmail(email);
	return usuario == null;
    }

    /**
     * @inheritDoc
     */
    @Override
    public boolean checkEmailAvailabilityPeluqueria(final String email) {
	final Usuario usuario = this.usuarioRepository.findByEmail(email);
	return usuario == null || usuario.getIdPeluqueria()
		.equals(UsuarioUtils.getUsuarioFromContext(this.usuarioRepository).getIdPeluqueria());
    }

    /**
     * @inheritDoc
     */
    @Override
    public void sendForgotEmail(final String to) throws InvalidUserException {
	final Usuario usuario = this.usuarioRepository.findByEmail(to);
	if (usuario == null) {
	    throw new InvalidUserException();
	}
	final String token = UUID.randomUUID().toString();
	final UsuarioTokenReset usuarioTokenReset = new UsuarioTokenReset(usuario.getIdUsuario(),
		token, new Date());
	this.usuarioTokenResetRepository.save(usuarioTokenReset);
	final String text = new StringBuilder(this.messages.get("email.body"))
		.append("\n ").append(this.urlWebappClient).append("/#/resetpassword?token=").append(token).toString();
	this.template.setText(text);
	this.emailService.sendSimpleMessage(to, this.messages.get("email.subject"), text);
    }

    /**
     * Checks if the token has expired according to its timestamp
     *
     * @param timestamp
     * @return
     */
    private static boolean tokenHasExpired(final Date timestamp) {
	return (new Date().getTime() - timestamp.getTime() > TOKEN_DURATION_MS);
    }

    /**
     * @inheritDoc
     */
    @Override
    public String resetPassword(final ResetPasswordDto resetPasswordDto) throws InvalidUserException {
	final UsuarioTokenReset usuarioTokenReset = this.usuarioTokenResetRepository
		.findByToken(resetPasswordDto.getToken());
	if (usuarioTokenReset == null
		|| tokenHasExpired(usuarioTokenReset.getTimestamp())) {
	    throw new InvalidUserException();
	}
	final Usuario usuario = usuarioTokenReset.getUsuario();
	usuario.setPassword(this.passwordEncoder.encode(resetPasswordDto.getPassword()));
	this.usuarioRepository.save(usuario);
	this.usuarioTokenResetRepository.delete(usuarioTokenReset);
	return usuario.getEmail();
    }

}
