package com.danielpid.hairstyle.service.impl;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.danielpid.hairstyle.entity.Cliente;
import com.danielpid.hairstyle.entity.Factura;
import com.danielpid.hairstyle.entity.LineaFactura;
import com.danielpid.hairstyle.entity.Usuario;
import com.danielpid.hairstyle.manager.api.ClienteManager;
import com.danielpid.hairstyle.manager.api.LineaFacturaManager;
import com.danielpid.hairstyle.repository.api.ClienteRepository;
import com.danielpid.hairstyle.repository.api.FacturaRepository;
import com.danielpid.hairstyle.repository.api.LineaFacturaRepository;
import com.danielpid.hairstyle.repository.api.UsuarioRepository;
import com.danielpid.hairstyle.service.api.ClienteDto;
import com.danielpid.hairstyle.service.api.FacturaCriteriaDto;
import com.danielpid.hairstyle.service.api.FacturaDto;
import com.danielpid.hairstyle.service.api.FacturaService;
import com.danielpid.hairstyle.service.api.LineaFacturaDto;
import com.danielpid.hairstyle.util.UsuarioUtils;
import com.danielpid.hairstyle.validation.ValidationUtils;

@Service("facturaService")
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
public class FacturaServiceImpl implements FacturaService {

    // ----- MANAGERs -----

    @Autowired
    private ClienteManager clienteManager;
    @Autowired
    private LineaFacturaManager lineaFacturaManager;

    // ----- REPOs -----

    @Autowired
    private FacturaRepository facturaRepository;
    @Autowired
    private LineaFacturaRepository lineaFacturaRepository;
    @Autowired
    private ClienteRepository clienteRepository;
    @Autowired
    private UsuarioRepository usuarioRepository;

    // ----- IMPLEMENTATION -----

    /**
     * @inheritDoc
     */
    @Override
    public List<FacturaDto> findByCriteria(final FacturaCriteriaDto criteria) {
	final Usuario usuario = UsuarioUtils.getUsuarioFromContext(this.usuarioRepository);
	criteria.setIdPeluqueria(usuario.getIdPeluqueria());
	final List<LineaFacturaDto> lineasFactura = this.facturaRepository.findByCriteria(criteria);
	final Map<Long, List<LineaFacturaDto>> idFacturaToListLineaFactura = lineasFactura.stream()
		.collect(Collectors.groupingBy(LineaFacturaDto::getIdFactura));
	return idFacturaToListLineaFactura.keySet().stream()
		.map(k -> idFacturaToListLineaFactura.get(k).get(0))
		.map(lineaFactura -> new FacturaDto(lineaFactura.getIdFactura(), lineaFactura.getIdCliente(),
			lineaFactura.getNombreCliente(), lineaFactura.getFechaAlta(), lineaFactura.getPrecioFactura(),
			idFacturaToListLineaFactura.get(lineaFactura.getIdFactura())))
		.sorted((f1, f2) -> Long.compare(f1.getIdFactura(), f2.getIdFactura()))
		.collect(Collectors.toList());
    }

    /**
     * @inheritDoc
     */
    @Override
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    public FacturaDto save(final FacturaDto dto) {
	final Cliente cliente = dto.getIdCliente() != null ? this.clienteRepository.findOne(dto.getIdCliente())
		: this.clienteManager.save(new ClienteDto(dto.getNombreCliente()));
	ValidationUtils.checkPeluqueria(cliente, this.usuarioRepository);
	final Factura factura = this.facturaRepository.save(new Factura(cliente, dto.getFechaAlta(), dto.getPrecio()));
	dto.setIdFactura(factura.getIdFactura());
	dto.setIdCliente(cliente.getIdCliente());
	dto.getDetalle().stream()
		.forEach(lf -> lf.setIdLineaFactura(this.lineaFacturaManager.saveLineaFactura(lf, factura)));
	return dto;
    }

    /**
     * @inheritDoc
     */
    @Override
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    public FacturaDto update(final FacturaDto dto) {
	final Factura factura = this.facturaRepository.findOne(dto.getIdFactura());
	this.updateFactura(dto, factura);
	this.updateLineaFactura(dto, factura);
	return dto;
    }

    /**
     * Actualiza la información de la factura
     *
     * @param dto
     */
    private void updateFactura(final FacturaDto dto, final Factura factura) {
	final Cliente cliente = this.clienteRepository.findOne(dto.getIdCliente());
	factura.setCliente(cliente);
	factura.setPrecio(dto.getPrecio());
    }

    /**
     * Actualiza las líneas de factura en base a lo que viene desde la vista y lo que hay en base de datos
     *
     * @param dto
     */
    private void updateLineaFactura(final FacturaDto dto, final Factura factura) {
	final List<LineaFactura> lineasFactura = this.lineaFacturaRepository
		.findByFacturaIdFacturaIn(dto.getIdFactura());
	// inserts
	dto.getDetalle().stream()
		// si no tienen id son nuevos
		.filter(ln -> ln.getIdLineaFactura() == null)
		.forEach(lfDto -> lfDto.setIdLineaFactura(this.lineaFacturaManager.saveLineaFactura(lfDto, factura)));
	// updates
	lineasFactura.stream()
		// si están en las 2 listas son actualizaciones
		.filter(lfEntity -> dto.getDetalle().stream()
			.map(LineaFacturaDto::getIdLineaFactura)
			.anyMatch(idLineaFactura -> idLineaFactura.equals(lfEntity.getIdLineaFactura())))
		.forEach(lfEntity -> this.lineaFacturaManager.updateLineaFactura(dto, lfEntity));
	// deletes
	lineasFactura.stream()
		.filter(lfEntity -> dto.getDetalle().stream()
			.filter(lfDto -> lfDto.getIdLineaFactura() != null)
			.map(LineaFacturaDto::getIdLineaFactura)
			.noneMatch(idLineaFactura -> idLineaFactura.equals(lfEntity.getIdLineaFactura())))
		.forEach(lfEntity -> this.lineaFacturaManager.deleteLineaFactura(lfEntity));
    }

    /**
     * @inheritDoc
     */
    @Override
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    public void removeFactura(final Long idFactura) {
	final List<LineaFactura> lineasFactura = this.lineaFacturaRepository.findByFacturaIdFacturaIn(idFactura);
	for (final LineaFactura lf : lineasFactura) {
	    this.lineaFacturaManager.deleteLineaFactura(lf);
	}
	this.facturaRepository.delete(idFactura);
    }

}
