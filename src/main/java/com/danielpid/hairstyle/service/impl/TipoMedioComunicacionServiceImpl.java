package com.danielpid.hairstyle.service.impl;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.danielpid.hairstyle.entity.TipoMedioComunicacionIdioma;
import com.danielpid.hairstyle.repository.api.TipoMedioComunicacionRepository;
import com.danielpid.hairstyle.service.api.TipoMedioComunicacionDto;
import com.danielpid.hairstyle.service.api.TipoMedioComunicacionService;

@Service("tipoMedioComunicacionService")
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
public class TipoMedioComunicacionServiceImpl implements TipoMedioComunicacionService {

    // ----- REPOs -----

    @Autowired
    private TipoMedioComunicacionRepository tipoMedioComunicacionRepository;

    // ----- IMPLEMENTATION -----

    /**
     * @inheritDoc
     */
    @Override
    public List<TipoMedioComunicacionDto> findByTipoMedioComunicacionPadre(final Short idIdioma,
	    final Short idTipoMedioComunicacionPadre) {
	final List<TipoMedioComunicacionIdioma> entities = this.tipoMedioComunicacionRepository
		.findByIdIdiomaAndTipoMedioComunicacionIdTipoMedioComunicacionPadre(idIdioma,
			idTipoMedioComunicacionPadre);
	return entities.stream().map(tipoMedioComunicacionIdioma -> new TipoMedioComunicacionDto(
		tipoMedioComunicacionIdioma.getIdTipoMedioComunicacion(), tipoMedioComunicacionIdioma.getNombre()))
		.collect(Collectors.toList());
    }

}
