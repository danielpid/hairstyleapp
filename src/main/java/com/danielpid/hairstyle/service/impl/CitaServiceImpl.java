package com.danielpid.hairstyle.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.danielpid.hairstyle.entity.Cita;
import com.danielpid.hairstyle.entity.Cliente;
import com.danielpid.hairstyle.entity.Usuario;
import com.danielpid.hairstyle.manager.api.ClienteManager;
import com.danielpid.hairstyle.repository.api.CitaRepository;
import com.danielpid.hairstyle.repository.api.ClienteRepository;
import com.danielpid.hairstyle.repository.api.UsuarioRepository;
import com.danielpid.hairstyle.service.api.CitaCriteriaDto;
import com.danielpid.hairstyle.service.api.CitaDto;
import com.danielpid.hairstyle.service.api.CitaService;
import com.danielpid.hairstyle.service.api.ClienteDto;
import com.danielpid.hairstyle.util.UsuarioUtils;
import com.danielpid.hairstyle.validation.ValidationUtils;

@Service("citaService")
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
public class CitaServiceImpl implements CitaService {

    // ----- MANAGERs -----

    @Autowired
    private ClienteManager clienteManager;

    // ----- REPOs -----

    @Autowired
    private CitaRepository citaRepository;
    @Autowired
    private ClienteRepository clienteRepository;
    @Autowired
    private UsuarioRepository usuarioRepository;

    // ----- IMPLEMENTATION -----

    /**
     * @inheritDoc
     */
    @Override
    public List<CitaDto> findByCriteria(final CitaCriteriaDto criteria) {
	final Usuario usuario = UsuarioUtils.getUsuarioFromContext(this.usuarioRepository);
	criteria.setIdPeluqueria(usuario.getIdPeluqueria());
	return this.citaRepository.findByCriteria(criteria);
    }

    /**
     * @inheritDoc
     */
    @Override
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    public CitaDto save(final CitaDto dto) {
	Cliente cliente = null;
	if (dto.getIdCliente() == null) {
	    cliente = this.clienteManager.save(new ClienteDto(dto.getNombreCliente()));
	} else {
	    cliente = this.clienteRepository.findOne(dto.getIdCliente());
	}
	final Cita entity = this.citaRepository
		.save(new Cita(cliente, dto.getFechaDesde(), dto.getFechaHasta(), dto.getObservaciones()));
	dto.setIdCita(entity.getIdCita());
	return dto;
    }

    /**
     * @inheritDoc
     */
    @Override
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    public CitaDto update(final CitaDto dto) {
	final Cita entity = this.citaRepository.findOne(dto.getIdCita());
	ValidationUtils.checkPeluqueria(entity, this.usuarioRepository);
	if (dto.getIdCliente() == null) {
	    final Cliente cliente = this.clienteManager.save(new ClienteDto(dto.getNombreCliente()));
	    dto.setIdCliente(cliente.getIdCliente());
	} else if (dto.getIdCliente() != entity.getCliente().getIdCliente()) {
	    final Cliente cliente = this.clienteRepository.findOne(dto.getIdCliente());
	    entity.setCliente(cliente);
	}
	entity.setFechaDesde(dto.getFechaDesde());
	entity.setFechaHasta(dto.getFechaHasta());
	this.citaRepository.save(entity);
	return dto;
    }

    /**
     * @inheritDoc
     */
    @Override
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    public void remove(final Long idCita) {
	this.citaRepository.delete(idCita);
    }

}
