package com.danielpid.hairstyle.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Component;

import com.danielpid.hairstyle.service.api.EmailService;

@Component("emailService")
public class EmailServiceImpl implements EmailService {

    @Autowired
    public JavaMailSender emailSender;

    /**
     * @inheritDoc
     */
    @Override
    public void sendSimpleMessage(final String to, final String subject, final String text) {
	final SimpleMailMessage message = new SimpleMailMessage();
	message.setTo(to);
	message.setSubject(subject);
	message.setText(text);
	this.emailSender.send(message);
    }

}
