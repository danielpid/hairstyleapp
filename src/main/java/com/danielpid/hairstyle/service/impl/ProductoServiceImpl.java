package com.danielpid.hairstyle.service.impl;

import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.danielpid.hairstyle.entity.Producto;
import com.danielpid.hairstyle.entity.Usuario;
import com.danielpid.hairstyle.repository.api.ProductoRepository;
import com.danielpid.hairstyle.repository.api.UsuarioRepository;
import com.danielpid.hairstyle.service.api.ProductoCriteriaDto;
import com.danielpid.hairstyle.service.api.ProductoDto;
import com.danielpid.hairstyle.service.api.ProductoService;
import com.danielpid.hairstyle.util.UsuarioUtils;
import com.danielpid.hairstyle.validation.ValidationUtils;

@Service("productoService")
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
public class ProductoServiceImpl implements ProductoService {

    // ----- REPOs -----

    @Autowired
    private ProductoRepository productoRepository;
    @Autowired
    private UsuarioRepository usuarioRepository;

    // ----- IMPLEMENTATION -----

    /**
     * @inheritDoc
     */
    @Override
    public List<ProductoDto> findAll() {
	final Usuario usuario = UsuarioUtils.getUsuarioFromContext(this.usuarioRepository);
	final List<Producto> entities = this.productoRepository
		.findByIdPeluqueriaAndBorradoFalseOrderByNombre(usuario.getIdPeluqueria());
	return entities.stream().map(producto -> new ProductoDto(producto.getIdProducto(), producto.getNombre(),
		producto.getPrecio(), producto.getStock())).collect(Collectors.toList());
    }

    /**
     * @inheritDoc
     */
    @Override
    public List<ProductoDto> findByCriteria(final ProductoCriteriaDto criteria) {
	final Usuario usuario = UsuarioUtils.getUsuarioFromContext(this.usuarioRepository);
	criteria.setIdPeluqueria(usuario.getIdPeluqueria());
	return this.productoRepository.findByCriteria(criteria);
    }

    /**
     * @inheritDoc
     */
    @Override
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    public ProductoDto save(final ProductoDto dto) {
	final Producto entity = this.productoRepository
		.save(new Producto(StringUtils.trimToNull(dto.getNombre()), dto.getPrecio(), dto.getStock()));
	dto.setIdProducto(entity.getIdProducto());
	return dto;
    }

    /**
     * @inheritDoc
     */
    @Override
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    public ProductoDto update(final ProductoDto dto) {
	final Producto entity = this.productoRepository.findOne(dto.getIdProducto());
	ValidationUtils.checkPeluqueria(entity, this.usuarioRepository);
	entity.setNombre(StringUtils.trimToNull(dto.getNombre()));
	entity.setPrecio(dto.getPrecio());
	entity.setStock(dto.getStock());
	this.productoRepository.save(entity);
	return dto;
    }

    /**
     * @inheritDoc
     */
    @Override
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    public void remove(final List<Integer> idsProducto) {
	this.productoRepository.deleteByIdProductoIn(idsProducto);
    }

}
