package com.danielpid.hairstyle.service.impl;

import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.danielpid.hairstyle.entity.Cliente;
import com.danielpid.hairstyle.entity.MedioComunicacion;
import com.danielpid.hairstyle.entity.Usuario;
import com.danielpid.hairstyle.manager.api.ClienteManager;
import com.danielpid.hairstyle.repository.api.ClienteRepository;
import com.danielpid.hairstyle.repository.api.MedioComunicacionRepository;
import com.danielpid.hairstyle.repository.api.UsuarioRepository;
import com.danielpid.hairstyle.service.api.ClienteCriteriaDto;
import com.danielpid.hairstyle.service.api.ClienteDto;
import com.danielpid.hairstyle.service.api.ClienteService;
import com.danielpid.hairstyle.util.TipoMedioComunicacionEnum;
import com.danielpid.hairstyle.util.UsuarioUtils;
import com.danielpid.hairstyle.validation.ValidationUtils;

@Service("clienteService")
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
public class ClienteServiceImpl implements ClienteService {

    // ----- MANAGERs -----

    @Autowired
    private ClienteManager clienteManager;

    // ----- REPOs -----

    @Autowired
    private ClienteRepository clienteRepository;
    @Autowired
    private UsuarioRepository usuarioRepository;
    @Autowired
    private MedioComunicacionRepository medioComunicacionRepository;

    // ----- IMPLEMENTATION -----

    /**
     * Agrupa por cliente cubriendo los medios de comunicación
     *
     * @param dtos
     * @return
     */
    private static List<ClienteDto> groupByCliente(final List<ClienteDto> dtos) {
	return dtos.stream().collect(Collectors.groupingBy(ClienteDto::getIdCliente,
		Collectors.reducing(ClienteDto::mergeMediosComunicacion)))
		.values().stream().map(optional -> optional.get())
		.sorted((c1, c2) -> c1.getNombre().compareToIgnoreCase(c2.getNombre()))
		.collect(Collectors.toList());
    }

    /**
     * @inheritDoc
     */
    @Override
    public List<ClienteDto> findAll() {
	final Usuario usuario = UsuarioUtils.getUsuarioFromContext(this.usuarioRepository);
	final List<ClienteDto> dtos = this.clienteRepository.findAll(usuario.getIdPeluqueria());
	return groupByCliente(dtos);
    }

    /**
     * @inheritDoc
     */
    @Override
    public List<ClienteDto> findByCriteria(final ClienteCriteriaDto criteria) {
	final Usuario usuario = UsuarioUtils.getUsuarioFromContext(this.usuarioRepository);
	criteria.setIdPeluqueria(usuario.getIdPeluqueria());
	final List<ClienteDto> dtos = this.clienteRepository.findByCriteria(criteria);
	return groupByCliente(dtos);
    }

    /**
     * @inheritDoc
     */
    @Override
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    public ClienteDto save(final ClienteDto dto) {
	final Cliente clienteNuevo = this.clienteManager.save(dto);
	dto.setIdCliente(clienteNuevo.getIdCliente());
	// móvil
	if (StringUtils.isNotBlank(dto.getDescripcionMedioComunicacionMovil())) {
	    final MedioComunicacion mc = this.saveMedioComunicacion(
		    TipoMedioComunicacionEnum.MOVIL.getIdTipoMedioComunicacion(),
		    StringUtils.trimToNull(dto.getDescripcionMedioComunicacionMovil()),
		    clienteNuevo.getIdCliente());
	    dto.setIdMedioComunicacionMovil(mc.getIdMedioComunicacion());
	}
	// casa
	if (StringUtils.isNotBlank(dto.getDescripcionMedioComunicacionCasa())) {
	    final MedioComunicacion mc = this.saveMedioComunicacion(
		    TipoMedioComunicacionEnum.CASA.getIdTipoMedioComunicacion(),
		    StringUtils.trimToNull(dto.getDescripcionMedioComunicacionCasa()),
		    clienteNuevo.getIdCliente());
	    dto.setIdMedioComunicacionCasa(mc.getIdMedioComunicacion());
	}
	// trabajo
	if (StringUtils.isNotBlank(dto.getDescripcionMedioComunicacionTrabajo())) {
	    final MedioComunicacion mc = this.saveMedioComunicacion(
		    TipoMedioComunicacionEnum.TRABAJO.getIdTipoMedioComunicacion(),
		    StringUtils.trimToNull(dto.getDescripcionMedioComunicacionTrabajo()),
		    clienteNuevo.getIdCliente());
	    dto.setIdMedioComunicacionTrabajo(mc.getIdMedioComunicacion());
	}
	// email
	if (StringUtils.isNotBlank(dto.getDescripcionMedioComunicacionEmail())) {
	    final MedioComunicacion mc = this.saveMedioComunicacion(
		    TipoMedioComunicacionEnum.EMAIL.getIdTipoMedioComunicacion(),
		    StringUtils.trimToNull(dto.getDescripcionMedioComunicacionEmail()),
		    clienteNuevo.getIdCliente());
	    dto.setIdMedioComunicacionEmail(mc.getIdMedioComunicacion());
	}
	return dto;
    }

    /**
     * @inheritDoc
     */
    @Override
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    public ClienteDto update(final ClienteDto dto) {
	final Cliente entity = this.clienteRepository.findOne(dto.getIdCliente());
	ValidationUtils.checkPeluqueria(entity, this.usuarioRepository);
	entity.setNombre(StringUtils.trimToNull(dto.getNombre()));
	entity.setObservaciones(StringUtils.trimToNull(dto.getObservaciones()));
	// movil
	final Integer idMedioComunicacionMovil = this.saveUpdateOrDelete(dto.getIdMedioComunicacionMovil(),
		TipoMedioComunicacionEnum.MOVIL.getIdTipoMedioComunicacion(),
		dto.getDescripcionMedioComunicacionMovil(), dto.getIdCliente());
	dto.setIdMedioComunicacionMovil(idMedioComunicacionMovil);
	// casa
	final Integer idMedioComunicacionCasa = this.saveUpdateOrDelete(dto.getIdMedioComunicacionCasa(),
		TipoMedioComunicacionEnum.CASA.getIdTipoMedioComunicacion(),
		dto.getDescripcionMedioComunicacionCasa(), dto.getIdCliente());
	dto.setIdMedioComunicacionCasa(idMedioComunicacionCasa);
	// trabajo
	final Integer idMedioComunicacionTrabajo = this.saveUpdateOrDelete(
		dto.getIdMedioComunicacionTrabajo(), TipoMedioComunicacionEnum.TRABAJO.getIdTipoMedioComunicacion(),
		dto.getDescripcionMedioComunicacionTrabajo(), dto.getIdCliente());
	dto.setIdMedioComunicacionTrabajo(idMedioComunicacionTrabajo);
	// email
	final Integer idMedioComunicacionEmail = this.saveUpdateOrDelete(dto.getIdMedioComunicacionEmail(),
		TipoMedioComunicacionEnum.EMAIL.getIdTipoMedioComunicacion(),
		dto.getDescripcionMedioComunicacionEmail(), dto.getIdCliente());
	dto.setIdMedioComunicacionEmail(idMedioComunicacionEmail);
	return dto;
    }

    /**
     * @inheritDoc
     */
    @Override
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    public void remove(final List<Integer> idsCliente) {
	final List<Cliente> clientesBorrar = this.clienteRepository.findByIdClienteIn(idsCliente);
	clientesBorrar.stream().forEach(c -> ValidationUtils.checkPeluqueria(c, this.usuarioRepository));
	clientesBorrar.stream().forEach(c -> c.setBorrado(true));
    }

    // ----- MEDIO COMUNICACION -----

    /**
     * Crea un medio de comunicación a partir de los parámetros
     *
     * @param idTipoMedioComunicacion
     * @param descripcionMedioComunicacion
     * @param idCliente
     * @return
     */
    private MedioComunicacion saveMedioComunicacion(final Short idTipoMedioComunicacion,
	    final String descripcionMedioComunicacion, final Integer idCliente) {
	return this.medioComunicacionRepository
		.save(new MedioComunicacion(idTipoMedioComunicacion, descripcionMedioComunicacion, idCliente));
    }

    /**
     * Inserta, actualiza o borra el medio de comunicación en función de los valores del dto y lo que hay en bd
     *
     * @param idMedioComunicacion
     * @param idTipoMedioComunicacion
     * @param descripcionMedioComunicacion
     * @param idCliente
     * @return
     */
    private Integer saveUpdateOrDelete(final Integer idMedioComunicacion, final Short idTipoMedioComunicacion,
	    final String descripcionMedioComunicacion, final Integer idCliente) {
	if (idMedioComunicacion == null && StringUtils.isNotBlank(descripcionMedioComunicacion)) {
	    // save
	    final MedioComunicacion mc = new MedioComunicacion(idTipoMedioComunicacion, descripcionMedioComunicacion,
		    idCliente);
	    this.medioComunicacionRepository.save(mc);
	    return mc.getIdMedioComunicacion();
	} else if (idMedioComunicacion != null && StringUtils.isBlank(descripcionMedioComunicacion)) {
	    // delete
	    this.medioComunicacionRepository.deleteByIdMedioComunicacion(idMedioComunicacion);
	} else if (idMedioComunicacion != null) {
	    final MedioComunicacion mc = this.medioComunicacionRepository.findOne(idMedioComunicacion);
	    if (!StringUtils.equals(descripcionMedioComunicacion, mc.getDescripcion())) {
		mc.setDescripcion(descripcionMedioComunicacion);
		this.medioComunicacionRepository.save(mc);
	    }
	    return idMedioComunicacion;
	}
	return null;
    }

}
