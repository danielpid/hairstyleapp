package com.danielpid.hairstyle.service.impl;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.danielpid.hairstyle.entity.Moneda;
import com.danielpid.hairstyle.entity.Peluqueria;
import com.danielpid.hairstyle.entity.Usuario;
import com.danielpid.hairstyle.entity.UsuarioRol;
import com.danielpid.hairstyle.repository.api.MonedaRepository;
import com.danielpid.hairstyle.repository.api.PeluqueriaRepository;
import com.danielpid.hairstyle.repository.api.UsuarioRepository;
import com.danielpid.hairstyle.repository.api.UsuarioRolRepository;
import com.danielpid.hairstyle.service.api.PeluqueriaDto;
import com.danielpid.hairstyle.service.api.PeluqueriaService;
import com.danielpid.hairstyle.util.RolEnum;
import com.danielpid.hairstyle.util.UsuarioUtils;
import com.danielpid.hairstyle.util.converters.PeluqueriaConverter;
import com.danielpid.hairstyle.validation.InvalidPasswordException;

@Service("peluqueriaService")
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
public class PeluqueriaServiceImpl implements PeluqueriaService {

    @Autowired
    private PasswordEncoder passwordEncoder;

    // ---- REPOs -----

    @Autowired
    private PeluqueriaRepository peluqueriaRepository;
    @Autowired
    private UsuarioRepository usuarioRepository;
    @Autowired
    private UsuarioRolRepository usuarioRolRepository;
    @Autowired
    private MonedaRepository monedaRepository;

    // ----- IMPLEMENTATION -----

    /**
     * @inheritDoc
     */
    @Override
    public PeluqueriaDto findPeluqueria() {
	final Usuario usuario = UsuarioUtils.getUsuarioFromContext(this.usuarioRepository);
	final Peluqueria peluqueria = this.peluqueriaRepository.findOne(usuario.getIdPeluqueria());
	return new PeluqueriaDto(peluqueria.getNombre(), usuario.getEmail(), peluqueria.getHoraApertura(),
		peluqueria.getHoraCierre(), peluqueria.getMoneda().getIdMoneda(), peluqueria.getMoneda().getSimbolo(),
		peluqueria.getMostrarTotalDiario());
    }

    /**
     * @inheritDoc
     */
    @Override
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    public PeluqueriaDto save(final PeluqueriaDto dto) {
	final Moneda moneda = this.monedaRepository.findOne(dto.getIdMoneda());
	final Peluqueria peluqueria = PeluqueriaConverter.toEntity(dto, moneda);
	this.peluqueriaRepository.save(peluqueria);
	final Usuario usuario = new Usuario(dto.getNombre(), this.passwordEncoder.encode(dto.getPasswordNueva()),
		dto.getEmail(), true, peluqueria.getIdPeluqueria());
	this.usuarioRepository.save(usuario);
	this.usuarioRolRepository.save(new UsuarioRol(usuario.getIdUsuario(), RolEnum.PROPIETARIO.getIdRol()));
	return dto;
    }

    /**
     * @inheritDoc
     */
    @Override
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false, rollbackFor = {
	    InvalidPasswordException.class })
    public PeluqueriaDto update(final PeluqueriaDto dto) throws InvalidPasswordException {
	final Usuario usuario = UsuarioUtils.getUsuarioFromContext(this.usuarioRepository);
	final Peluqueria peluqueria = this.peluqueriaRepository.findOne(usuario.getIdPeluqueria());
	final Moneda moneda = this.monedaRepository.findOne(dto.getIdMoneda());
	PeluqueriaConverter.updateEntity(dto, peluqueria, moneda);
	usuario.setNombre(dto.getNombre());
	// update email
	if (!StringUtils.equalsIgnoreCase(usuario.getEmail(), dto.getEmail())) {
	    this.updateEmail(dto, usuario);
	}
	// update password
	if (StringUtils.isNotBlank(dto.getPasswordActual())
		&& StringUtils.isNotBlank(dto.getPasswordNueva())) {
	    this.updatePassword(dto, usuario);
	}
	dto.setSimboloMoneda(moneda.getSimbolo());
	return dto;
    }

    /**
     * Actualiza el email del usuario
     *
     * @param dto
     * @param usuario
     * @throws InvalidPasswordException
     *             si la password introducida por el usuario es incorrecta
     */
    private void updateEmail(final PeluqueriaDto dto, final Usuario usuario) throws InvalidPasswordException {
	if (!this.passwordEncoder.matches(dto.getPasswordEmail(), usuario.getPassword())) {
	    throw new InvalidPasswordException();
	}
	usuario.setEmail(dto.getEmail());
	dto.setEmailActualizado(true);
    }

    /**
     * Actualiza la contraseña
     *
     * @param dto
     * @param usuario
     * @throws InvalidPasswordException
     *             si la password introducida por el usuario es incorrecta
     */
    private void updatePassword(final PeluqueriaDto dto, final Usuario usuario) throws InvalidPasswordException {
	if (!this.passwordEncoder.matches(dto.getPasswordActual(), usuario.getPassword())) {
	    throw new InvalidPasswordException();
	}
	usuario.setPassword(this.passwordEncoder.encode(dto.getPasswordNueva()));
    }

    /**
     * @inheritDoc
     */
    @Override
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    public void delete(final String email) {
	final Usuario usuario = this.usuarioRepository.findByEmail(email);
	this.usuarioRolRepository.deleteByIdUsuario(usuario.getIdUsuario());
	this.usuarioRepository.delete(usuario);
	this.peluqueriaRepository.deleteByIdPeluqueria(usuario.getIdPeluqueria());
    }

}
