package com.danielpid.hairstyle.service.api;

import java.util.List;

public interface FacturaService {

    /**
     * Recupera las facturas, con sus respectivas líneas de factura, que cumplen con los criterios establecidos
     *
     * @param criteria
     * @return
     */
    List<FacturaDto> findByCriteria(FacturaCriteriaDto criteria);

    /**
     * Crea un nueva factura junto con todas sus líneas de factura
     *
     * @param dto
     * @return
     */
    FacturaDto save(FacturaDto dto);

    /**
     * Actualiza la información de la factura y sus líneas de factura
     * 
     * @param dto
     * @return
     */
    FacturaDto update(FacturaDto dto);

    /**
     * Elimina la factua junto con todas sus líneas de factura
     *
     * @param idFactura
     */
    void removeFactura(Long idFactura);

}
