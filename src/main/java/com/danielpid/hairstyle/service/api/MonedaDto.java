package com.danielpid.hairstyle.service.api;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class MonedaDto {

    private Short idMoneda;
    private String simbolo;
    private String nombre;

}
