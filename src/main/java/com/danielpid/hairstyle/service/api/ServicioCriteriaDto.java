package com.danielpid.hairstyle.service.api;

import java.util.Set;

import com.danielpid.hairstyle.util.CriteriaDto;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class ServicioCriteriaDto extends CriteriaDto {

    private String nombre;
    private Float precio;
    private Set<Integer> idsExcluir;

}
