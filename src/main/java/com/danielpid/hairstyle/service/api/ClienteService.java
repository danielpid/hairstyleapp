package com.danielpid.hairstyle.service.api;

import java.util.List;

public interface ClienteService {

    /**
     * Recupera todos los clientes de la peluquería del usuario
     *
     * @return
     */
    List<ClienteDto> findAll();

    /**
     * Recupera los clientes que cumplen con los clientes dados
     *
     * @param criteria
     * @return
     */
    List<ClienteDto> findByCriteria(ClienteCriteriaDto criteria);

    /**
     * Crea un nuevo cliente y sus medios de comunicación en base de datos
     *
     * @param dto
     * @return
     */
    ClienteDto save(ClienteDto dto);

    /**
     * Actualiza un cliente y sus medios de comunicación
     *
     * @param dto
     * @return
     */
    ClienteDto update(ClienteDto dto);

    /**
     * Borrado lógico de los clientes dados
     *
     * @param idsCliente
     */
    void remove(List<Integer> idsCliente);

}
