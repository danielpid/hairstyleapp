package com.danielpid.hairstyle.service.api;

import java.util.Date;

import com.danielpid.hairstyle.util.CriteriaDto;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
public class CitaCriteriaDto extends CriteriaDto {

    private Date fechaDesde;
    private Date fechaHasta;

}
