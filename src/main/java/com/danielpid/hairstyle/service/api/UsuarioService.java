package com.danielpid.hairstyle.service.api;

import com.danielpid.hairstyle.validation.InvalidUserException;

public interface UsuarioService {

    /**
     * Returns true if the given email address is available, false otherwise
     *
     * @param email
     * @return
     */
    boolean checkEmailAvailability(String email);

    /**
     * Returns true if the given email address is available for the current hairdresser, false otherwise
     *
     * @param email
     * @return
     */
    boolean checkEmailAvailabilityPeluqueria(String email);

    /**
     * Sends an email to reset the password
     *
     * @param to
     *            the email of the user
     * @throws InvalidUserException
     *             When the email given does not exist in the database
     */
    void sendForgotEmail(String to) throws InvalidUserException;

    /**
     * Sets the new password for the user identified by the token
     *
     * @param resetPasswordDto
     * @return the email of the user
     * @throws InvalidUserException
     */
    String resetPassword(ResetPasswordDto resetPasswordDto) throws InvalidUserException;

}
