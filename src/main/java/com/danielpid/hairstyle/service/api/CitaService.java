package com.danielpid.hairstyle.service.api;

import java.util.List;

public interface CitaService {

    /**
     * Recupera las citas que cumplen con los criterios
     *
     * @param criteria
     * @return
     */
    List<CitaDto> findByCriteria(CitaCriteriaDto criteria);

    /**
     * Crea una nueva cita en la base de datos
     *
     * @param dto
     * @return
     */
    CitaDto save(CitaDto dto);

    /**
     * Actualiza la cita en la base de datos
     *
     * @param dto
     * @return
     */
    CitaDto update(CitaDto dto);

    /**
     * Borra la cita con el id dado
     *
     * @param idCita
     */
    void remove(Long idCita);

}
