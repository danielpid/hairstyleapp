package com.danielpid.hairstyle.service.api;

import java.io.Serializable;

import javax.validation.constraints.DecimalMax;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.Digits;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotEmpty;

import lombok.Data;

@Data
@SuppressWarnings("serial")
public class ServicioDto implements Serializable {

    private Integer idServicio;
    @NotEmpty(message = "{nombre.notempty}")
    @Size(max = 100, message = "{nombre.max}")
    private String nombre;
    @NotNull(message = "{precio.notnull}")
    @DecimalMin(value = "0.01", message = "{precio.decimalmin}")
    @DecimalMax(value = "999999.99", message = "{precio.decimalmax}")
    @Digits(integer = 6, fraction = 2, message = "{digitos.fuera.rango}")
    private Float precio;

    public ServicioDto() {
	super();
    }

    public ServicioDto(final Integer idServicio, final String nombre, final Float precio) {
	super();
	this.idServicio = idServicio;
	this.nombre = nombre;
	this.precio = precio;
    }

}
