package com.danielpid.hairstyle.service.api;

import java.io.Serializable;

import lombok.Data;

@SuppressWarnings("serial")
@Data
public class TipoMedioComunicacionDto implements Serializable {

    private Short idTipoMedioComunicacion;
    private String nombre;

    public TipoMedioComunicacionDto() {
	super();
    }

    public TipoMedioComunicacionDto(final Short idTipoMedioComunicacion, final String nombre) {
	super();
	this.idTipoMedioComunicacion = idTipoMedioComunicacion;
	this.nombre = nombre;
    }

}
