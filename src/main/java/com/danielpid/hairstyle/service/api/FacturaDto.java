package com.danielpid.hairstyle.service.api;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.validation.constraints.DecimalMax;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.Digits;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotEmpty;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@SuppressWarnings("serial")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class FacturaDto implements Serializable {

    private Long idFactura;
    // Puede crearse el cliente en el momento de crear la cita
    private Integer idCliente;
    @Size(max = 100, message = "{nombre.max}")
    private String nombreCliente;
    @NotNull
    private Date fechaAlta;
    @NotNull(message = "{precio.notnull}")
    @DecimalMin(value = "0.01", message = "{precio.decimalmin}")
    @DecimalMax(value = "999999.99", message = "{precio.decimalmax}")
    @Digits(integer = 6, fraction = 2, message = "{digitos.fuera.rango}")
    private Float precio;
    @NotEmpty(message = "{detalle.factura.notempty}")
    private List<LineaFacturaDto> detalle;

}
