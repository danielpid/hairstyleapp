package com.danielpid.hairstyle.service.api;

import java.io.Serializable;
import java.util.Date;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import lombok.Data;

@Data
@SuppressWarnings("serial")
public class CitaDto implements Serializable {

    private Long idCita;
    // Puede crearse el cliente en el momento de crear la cita
    private Integer idCliente;
    @Size(max = 100, message = "{nombre.max}")
    private String nombreCliente;
    @NotNull
    private Date fechaDesde;
    @NotNull
    private Date fechaHasta;
    @Size(max = 255, message = "{observaciones.max}")
    private String observaciones;

}
