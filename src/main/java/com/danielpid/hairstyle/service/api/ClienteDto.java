package com.danielpid.hairstyle.service.api;

import java.io.Serializable;

import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotEmpty;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@SuppressWarnings("serial")
public class ClienteDto implements Serializable {

    private Integer idCliente;
    @NotEmpty(message = "{nombre.notempty}")
    @Size(max = 100, message = "{nombre.max}")
    private String nombre;
    @Size(max = 255, message = "{observaciones.max}")
    private String observaciones;
    // Medios comunicacion
    private Integer idMedioComunicacionMovil;
    @Size(max = 100, message = "{medio.comunicacion.max}")
    private String descripcionMedioComunicacionMovil;
    private Integer idMedioComunicacionCasa;
    @Size(max = 100, message = "{medio.comunicacion.max}")
    private String descripcionMedioComunicacionCasa;
    private Integer idMedioComunicacionTrabajo;
    @Size(max = 100, message = "{medio.comunicacion.max}")
    private String descripcionMedioComunicacionTrabajo;
    private Integer idMedioComunicacionEmail;
    @Size(max = 100, message = "{medio.comunicacion.max}")
    private String descripcionMedioComunicacionEmail;

    public ClienteDto(final String nombre) {
	super();
	this.nombre = nombre;
    }

    public ClienteDto(final Integer idCliente, final String nombre) {
	super();
	this.idCliente = idCliente;
	this.nombre = nombre;
    }

    public static ClienteDto mergeMediosComunicacion(final ClienteDto dst, final ClienteDto src) {
	if (src.getIdMedioComunicacionMovil() != null) {
	    dst.setIdMedioComunicacionMovil(src.getIdMedioComunicacionMovil());
	    dst.setDescripcionMedioComunicacionMovil(src.getDescripcionMedioComunicacionMovil());
	} else if (src.getIdMedioComunicacionCasa() != null) {
	    dst.setIdMedioComunicacionCasa(src.getIdMedioComunicacionCasa());
	    dst.setDescripcionMedioComunicacionCasa(src.getDescripcionMedioComunicacionCasa());
	} else if (src.getIdMedioComunicacionTrabajo() != null) {
	    dst.setIdMedioComunicacionTrabajo(src.getIdMedioComunicacionTrabajo());
	    dst.setDescripcionMedioComunicacionTrabajo(src.getDescripcionMedioComunicacionTrabajo());
	} else if (src.getIdMedioComunicacionEmail() != null) {
	    dst.setIdMedioComunicacionEmail(src.getIdMedioComunicacionEmail());
	    dst.setDescripcionMedioComunicacionEmail(src.getDescripcionMedioComunicacionEmail());
	}
	return dst;
    }

}
