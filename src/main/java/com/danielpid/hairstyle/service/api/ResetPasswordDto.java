package com.danielpid.hairstyle.service.api;

import java.io.Serializable;

import javax.validation.constraints.NotNull;

import lombok.Data;

@Data
@SuppressWarnings("serial")
public class ResetPasswordDto implements Serializable {

    @NotNull
    private String token;
    @NotNull
    private String password;

}
