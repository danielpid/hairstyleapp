package com.danielpid.hairstyle.service.api;

import java.util.List;

public interface ProductoService {

    /**
     * Recupera todos los productos de la peluquería
     *
     * @return
     */
    List<ProductoDto> findAll();

    /**
     * Recupera los productos que cumplen los criterios dados
     *
     * @param criteria
     * @return
     */
    List<ProductoDto> findByCriteria(ProductoCriteriaDto criteria);

    /**
     * Crea un nuevo producto
     *
     * @param dto
     * @return
     */
    ProductoDto save(ProductoDto dto);

    /**
     * Actualiza un producto
     *
     * @param dto
     * @return
     */
    ProductoDto update(ProductoDto dto);

    /**
     * Borra los productos
     *
     * @param idsProducto
     */
    void remove(List<Integer> idsProducto);

}
