package com.danielpid.hairstyle.service.api;

import java.util.List;

public interface MonedaService {

    /**
     * Devuelva todas las monedas disponibles
     * 
     * @return
     */
    List<MonedaDto> findAll();

}
