package com.danielpid.hairstyle.service.api;

import java.io.Serializable;

import javax.validation.constraints.DecimalMax;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.Digits;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotEmpty;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@SuppressWarnings("serial")
public class ProductoDto implements Serializable {

    private Integer idProducto;
    @NotEmpty(message = "{nombre.notempty}")
    @Size(max = 100, message = "{nombre.max}")
    private String nombre;
    @NotNull(message = "{precio.notnull}")
    @DecimalMin(value = "0.01", message = "{precio.decimalmin}")
    @DecimalMax(value = "999999.99", message = "{precio.decimalmax}")
    @Digits(integer = 6, fraction = 2, message = "{digitos.fuera.rango}")
    private Float precio;
    @Min(value = 1, message = "{stock.min}")
    @Max(value = 999999, message = "{stock.max")
    private Integer stock;

    public ProductoDto(final Integer idProducto, final String nombre, final Float precio, final Integer stock) {
	super();
	this.idProducto = idProducto;
	this.nombre = nombre;
	this.precio = precio;
	this.stock = stock;
    }

}
