package com.danielpid.hairstyle.service.api;

import java.util.List;

public interface ServicioService {

    /**
     * Recupera todos los servicios de la peluquería
     *
     * @return
     */
    List<ServicioDto> findAll();

    /**
     * Recupera los servicios que cumplen los criterios dados
     * 
     * @param criteria
     * @return
     */
    List<ServicioDto> findByCriteria(ServicioCriteriaDto criteria);

    /**
     * Crea un nuevo servicio
     *
     * @param dto
     * @return
     */
    ServicioDto save(ServicioDto dto);

    /**
     * Actualiza un servicio
     *
     * @param dto
     * @return
     */
    ServicioDto update(ServicioDto dto);

    /**
     * Borra los servicios
     *
     * @param idsServicio
     */
    void remove(List<Integer> idsServicio);

}
