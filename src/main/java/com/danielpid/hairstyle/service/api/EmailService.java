package com.danielpid.hairstyle.service.api;

public interface EmailService {

    /**
     * Sends an email with the given params
     *
     * @param to
     * @param subject
     * @param text
     */
    void sendSimpleMessage(String to, String subject, String text);

}
