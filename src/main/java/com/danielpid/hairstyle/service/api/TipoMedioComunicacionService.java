package com.danielpid.hairstyle.service.api;

import java.util.List;

public interface TipoMedioComunicacionService {

    /**
     * Recupera los tipos de medio de comunicación que tienen como padre al dado
     * 
     * @param idIdioma
     * @param idTipoMedioComunicacionPadre
     * @return
     */
    List<TipoMedioComunicacionDto> findByTipoMedioComunicacionPadre(Short idIdioma, Short idTipoMedioComunicacionPadre);

}
