package com.danielpid.hairstyle.service.api;

public interface BelongsToPeluqueria {

    /**
     * Devuelve a la peluquería a la que pertenece el objeto que implementa la interface
     * 
     * @return
     */
    Integer getIdPeluqueria();

}
