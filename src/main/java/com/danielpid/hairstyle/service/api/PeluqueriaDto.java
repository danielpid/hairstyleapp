package com.danielpid.hairstyle.service.api;

import java.io.Serializable;
import java.time.LocalTime;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotEmpty;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@SuppressWarnings("serial")
public class PeluqueriaDto implements Serializable {

    @NotEmpty(message = "{nombre.notempty}")
    @Size(max = 100, message = "{nombre.max}")
    private String nombre;
    @NotNull(message = "{email.notnull}")
    @Size(max = 100, message = "{email.maxsize")
    private String email;
    private String passwordEmail;
    private Boolean emailActualizado;
    @NotNull(message = "{hora.apertura.notnull}")
    private LocalTime horaApertura;
    @NotNull(message = "{hora.cierre.notnull}")
    private LocalTime horaCierre;
    @Size(min = 6, max = 40, message = "{password.size}")
    private String passwordActual;
    @Size(min = 6, max = 40, message = "{password.size}")
    private String passwordNueva;
    @NotNull(message = "{moneda.notnull}")
    private Short idMoneda;
    private String simboloMoneda;
    private Boolean mostrarTotalDiario;

    public PeluqueriaDto(final String nombre, final String email, final LocalTime horaApertura,
	    final LocalTime horaCierre, final Short idMoneda, final String simboloMoneda,
	    final Boolean mostrarTotalDiario) {
	super();
	this.nombre = nombre;
	this.email = email;
	this.horaApertura = horaApertura;
	this.horaCierre = horaCierre;
	this.idMoneda = idMoneda;
	this.simboloMoneda = simboloMoneda;
	this.mostrarTotalDiario = mostrarTotalDiario;
    }

}
