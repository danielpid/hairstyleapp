package com.danielpid.hairstyle.service.api;

import com.danielpid.hairstyle.util.CriteriaDto;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class ClienteCriteriaDto extends CriteriaDto {

    private String nombre;
    private String observaciones;
    private String descripcionMedioComunicacion;

}
