package com.danielpid.hairstyle.service.api;

import java.util.List;
import java.util.Set;

import com.danielpid.hairstyle.util.CriteriaDto;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class ProductoCriteriaDto extends CriteriaDto {

    private List<Integer> idsProducto;
    private String nombre;
    private Float precio;
    private Integer stock;
    private Set<Integer> idsExcluir;

}
