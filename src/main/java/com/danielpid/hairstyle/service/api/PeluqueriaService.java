package com.danielpid.hairstyle.service.api;

import com.danielpid.hairstyle.validation.InvalidPasswordException;

public interface PeluqueriaService {

    /**
     * Recupera la peluqueria actual
     *
     * @return
     */
    PeluqueriaDto findPeluqueria();

    /**
     * Crea una nueva peluquería con un usuario asociado
     *
     * @return
     */
    PeluqueriaDto save(PeluqueriaDto dto);

    /**
     * Actualiza la información de la peluquería
     *
     * @param dto
     * @return
     * @throws InvalidPasswordException
     */
    PeluqueriaDto update(PeluqueriaDto dto) throws InvalidPasswordException;

    /**
     * Elimina la peluqueria y usuario con el email dado
     * 
     * @param email
     */
    void delete(String email);

}
