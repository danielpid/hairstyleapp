package com.danielpid.hairstyle.service.api;

import java.util.Date;

import com.danielpid.hairstyle.util.CriteriaDto;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class FacturaCriteriaDto extends CriteriaDto {

    private Date fechaDesde;
    private Date fechaHasta;

}
