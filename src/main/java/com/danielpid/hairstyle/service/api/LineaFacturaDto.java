package com.danielpid.hairstyle.service.api;

import java.io.Serializable;
import java.util.Date;

import javax.validation.constraints.DecimalMax;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.Digits;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import lombok.Data;

@SuppressWarnings("serial")
@Data
public class LineaFacturaDto implements Serializable {

    private Long idFactura;
    private Integer idCliente;
    private String nombreCliente;
    private Date fechaAlta;
    private Float precioFactura;
    private Long idLineaFactura;
    private Integer idProducto;
    private String nombreProducto;
    private Integer idServicio;
    private String nombreServicio;
    @NotNull(message = "{cantidad.notnull}")
    @Min(value = 1, message = "{cantidad.min}")
    @Max(value = 999999, message = "{cantidad.max")
    private Short cantidad;
    @NotNull(message = "{precio.notnull}")
    @DecimalMin(value = "0.01", message = "{precio.decimalmin}")
    @DecimalMax(value = "999999.99", message = "{precio.decimalmax}")
    @Digits(integer = 6, fraction = 2, message = "{digitos.fuera.rango}")
    private Float precioUnidad;
    @NotNull(message = "{precio.notnull}")
    @DecimalMin(value = "0.01", message = "{precio.decimalmin}")
    @DecimalMax(value = "999999.99", message = "{precio.decimalmax}")
    @Digits(integer = 6, fraction = 2, message = "{digitos.fuera.rango}")
    private Float precioLineaFactura;

}
