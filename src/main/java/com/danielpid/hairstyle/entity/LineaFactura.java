package com.danielpid.hairstyle.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@Entity
public class LineaFactura {

    private @Id @GeneratedValue Long idLineaFactura;
    @ManyToOne
    @JoinColumn(name = "id_factura")
    private Factura factura;
    @ManyToOne
    @JoinColumn(name = "id_producto")
    private Producto producto;
    @ManyToOne
    @JoinColumn(name = "id_servicio")
    private Servicio servicio;
    private Float precio;
    private Short cantidad;

    public LineaFactura(final Factura factura, final Producto producto, final Servicio servicio, final Float precio,
	    final Short cantidad) {
	super();
	this.factura = factura;
	this.producto = producto;
	this.servicio = servicio;
	this.precio = precio;
	this.cantidad = cantidad;
    }

}
