package com.danielpid.hairstyle.entity;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@Entity
public class UsuarioTokenReset {

    private @Id @GeneratedValue Integer idUsuarioTokenReset;
    @ManyToOne
    @JoinColumn(name = "id_usuario")
    private Usuario usuario;
    private String token;
    private Date timestamp;

    public UsuarioTokenReset(final Integer idUsuario, final String token, final Date timestamp) {
	super();
	this.usuario = new Usuario(idUsuario);
	this.token = token;
	this.timestamp = timestamp;
    }

}
