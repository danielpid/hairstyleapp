package com.danielpid.hairstyle.entity;

import java.time.LocalTime;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import lombok.Data;

@Data
@Entity
public class Peluqueria {

    private @Id @GeneratedValue Integer idPeluqueria;
    private String nombre;
    private LocalTime horaApertura;
    private LocalTime horaCierre;
    @ManyToOne
    @JoinColumn(name = "id_moneda")
    private Moneda moneda;
    private Boolean mostrarTotalDiario;

}
