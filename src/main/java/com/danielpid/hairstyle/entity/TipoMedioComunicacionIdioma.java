package com.danielpid.hairstyle.entity;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import lombok.Data;

@Data
@Entity
@IdClass(TipoMedioComunicacionIdiomaId.class)
public class TipoMedioComunicacionIdioma {

    private @Id Short idTipoMedioComunicacion;
    private @Id Short idIdioma;
    private String nombre;
    @ManyToOne
    @JoinColumn(name = "idTipoMedioComunicacion", insertable = false, updatable = false)
    private TipoMedioComunicacion tipoMedioComunicacion;

}
