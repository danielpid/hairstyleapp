package com.danielpid.hairstyle.entity;

import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import com.danielpid.hairstyle.eventhandler.ServicioEntityListener;
import com.danielpid.hairstyle.service.api.BelongsToPeluqueria;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@Entity
@EntityListeners({ ServicioEntityListener.class })
public class Servicio implements BelongsToPeluqueria {

    private @Id @GeneratedValue Integer idServicio;
    private Integer idPeluqueria;
    private String nombre;
    private Float precio;
    private boolean borrado;

    public Servicio(final Integer idServicio) {
	super();
	this.idServicio = idServicio;
    }

    public Servicio(final String nombre, final Float precio) {
	super();
	this.nombre = nombre;
	this.precio = precio;
    }

}
