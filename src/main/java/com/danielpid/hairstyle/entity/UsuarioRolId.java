package com.danielpid.hairstyle.entity;

import java.io.Serializable;

import lombok.Data;

@SuppressWarnings("serial")
@Data
public class UsuarioRolId implements Serializable {

    private Integer idUsuario;
    private Short idRol;

}
