package com.danielpid.hairstyle.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import lombok.Data;

@Data
@Entity
public class Moneda {

    private @Id @GeneratedValue Short idMoneda;
    private String simbolo;
    private String nombre;

}
