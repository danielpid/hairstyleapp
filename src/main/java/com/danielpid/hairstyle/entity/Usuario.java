package com.danielpid.hairstyle.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@ToString(exclude = "password")
@NoArgsConstructor
@Entity
public class Usuario {

    private @Id @GeneratedValue Integer idUsuario;
    private String nombre;
    private @JsonIgnore String password;
    private String email;
    private boolean enabled;
    private Integer idPeluqueria;

    public Usuario(final Integer idUsuario) {
	super();
	this.idUsuario = idUsuario;
    }

    public Usuario(final String nombre, final String password, final String email, final boolean enabled,
	    final Integer idPeluqueria) {
	super();
	this.nombre = nombre;
	this.password = password;
	this.email = email;
	this.enabled = enabled;
	this.idPeluqueria = idPeluqueria;
    }

}
