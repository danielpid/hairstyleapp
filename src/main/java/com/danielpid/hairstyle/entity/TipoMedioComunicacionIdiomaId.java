package com.danielpid.hairstyle.entity;

import java.io.Serializable;

import lombok.Data;

@SuppressWarnings("serial")
@Data
public class TipoMedioComunicacionIdiomaId implements Serializable {
    private Short idTipoMedioComunicacion;
    private Short idIdioma;
}
