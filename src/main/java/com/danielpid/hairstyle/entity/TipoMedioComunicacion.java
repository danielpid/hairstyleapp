package com.danielpid.hairstyle.entity;

import javax.persistence.Entity;
import javax.persistence.Id;

import lombok.Data;

@Data
@Entity
public class TipoMedioComunicacion {

    private @Id Short idTipoMedioComunicacion;
    private String nombre;
    private Short idTipoMedioComunicacionPadre;

}
