package com.danielpid.hairstyle.entity;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.danielpid.hairstyle.eventhandler.FacturaEntityListener;
import com.danielpid.hairstyle.service.api.BelongsToPeluqueria;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@Entity
@EntityListeners({ FacturaEntityListener.class })
public class Factura implements BelongsToPeluqueria {

    private @Id @GeneratedValue Long idFactura;
    @ManyToOne
    @JoinColumn(name = "id_cliente")
    private Cliente cliente;
    private Date fechaAlta;
    private Float precio;

    public Factura(final Cliente cliente, final Date fechaAlta, final Float precio) {
	super();
	this.cliente = cliente;
	this.fechaAlta = fechaAlta;
	this.precio = precio;
    }

    @Override
    public Integer getIdPeluqueria() {
	return this.cliente.getIdPeluqueria();
    }

}
