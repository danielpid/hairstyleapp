package com.danielpid.hairstyle.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import lombok.Data;

@Data
@Entity
public class MedioComunicacion {

    private @Id @GeneratedValue Integer idMedioComunicacion;
    private Short idTipoMedioComunicacion;
    private String descripcion;
    private Integer idCliente;

    public MedioComunicacion() {
	super();
    }

    public MedioComunicacion(final Short idTipoMedioComunicacion, final String descripcion, final Integer idCliente) {
	super();
	this.idTipoMedioComunicacion = idTipoMedioComunicacion;
	this.descripcion = descripcion;
	this.idCliente = idCliente;
    }

}
