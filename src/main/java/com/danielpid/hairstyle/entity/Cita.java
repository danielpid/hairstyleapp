package com.danielpid.hairstyle.entity;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.danielpid.hairstyle.eventhandler.CitaEntityListener;
import com.danielpid.hairstyle.service.api.BelongsToPeluqueria;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@Entity
@EntityListeners({ CitaEntityListener.class })
public class Cita implements BelongsToPeluqueria {

    private @Id @GeneratedValue Long idCita;
    @ManyToOne
    @JoinColumn(name = "id_cliente")
    private Cliente cliente;
    private Date fechaDesde;
    private Date fechaHasta;
    private String observaciones;

    public Cita(final Cliente cliente, final Date fechaDesde, final Date fechaHasta, final String observaciones) {
	super();
	this.cliente = cliente;
	this.fechaDesde = fechaDesde;
	this.fechaHasta = fechaHasta;
	this.observaciones = observaciones;
    }

    @Override
    public Integer getIdPeluqueria() {
	return this.cliente.getIdPeluqueria();
    }

}
