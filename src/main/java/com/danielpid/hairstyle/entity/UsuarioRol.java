package com.danielpid.hairstyle.entity;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@IdClass(UsuarioRolId.class)
public class UsuarioRol {

    private @Id Integer idUsuario;
    private @Id Short idRol;

}
