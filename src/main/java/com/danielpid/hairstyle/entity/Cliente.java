package com.danielpid.hairstyle.entity;

import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import com.danielpid.hairstyle.eventhandler.ClienteEntityListener;
import com.danielpid.hairstyle.service.api.BelongsToPeluqueria;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@Entity
@EntityListeners({ ClienteEntityListener.class })
public class Cliente implements BelongsToPeluqueria {

    private @Id @GeneratedValue Integer idCliente;
    private Integer idPeluqueria;
    private String nombre;
    private String observaciones;
    private boolean borrado;

    public Cliente(final Integer idCliente) {
	super();
	this.idCliente = idCliente;
    }

    public Cliente(final String nombre, final String observaciones) {
	super();
	this.nombre = nombre;
	this.observaciones = observaciones;
    }

}
