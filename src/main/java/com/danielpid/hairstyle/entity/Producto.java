package com.danielpid.hairstyle.entity;

import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import com.danielpid.hairstyle.eventhandler.ProductoEntityListener;
import com.danielpid.hairstyle.service.api.BelongsToPeluqueria;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@Entity
@EntityListeners({ ProductoEntityListener.class })
public class Producto implements BelongsToPeluqueria {

    private @Id @GeneratedValue Integer idProducto;
    private Integer idPeluqueria;
    private String nombre;
    private Float precio;
    private Integer stock;
    private boolean borrado;

    public Producto(final Integer idProducto) {
	super();
	this.idProducto = idProducto;
    }

    public Producto(final String nombre, final Float precio, final Integer stock) {
	super();
	this.nombre = nombre;
	this.precio = precio;
	this.stock = stock;
    }

}
