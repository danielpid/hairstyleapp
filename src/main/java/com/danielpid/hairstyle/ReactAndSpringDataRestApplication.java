package com.danielpid.hairstyle;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.orm.jpa.vendor.HibernateJpaSessionFactoryBean;

@SpringBootApplication
public class ReactAndSpringDataRestApplication {

    @Bean
    public HibernateJpaSessionFactoryBean sessionFactory() {
	return new HibernateJpaSessionFactoryBean();
    }

    @Bean
    public SimpleMailMessage templateSimpleMessage() {
	final SimpleMailMessage message = new SimpleMailMessage();
	return message;
    }

    public static void main(final String[] args) {
	SpringApplication.run(ReactAndSpringDataRestApplication.class, args);
    }
}
