package com.danielpid.hairstyle.eventhandler;

import javax.persistence.PrePersist;
import javax.persistence.PreRemove;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.stereotype.Component;

import com.danielpid.hairstyle.entity.Cliente;
import com.danielpid.hairstyle.entity.Usuario;
import com.danielpid.hairstyle.repository.api.UsuarioRepository;
import com.danielpid.hairstyle.util.UsuarioUtils;

@Component
public class ClienteEntityListener {

    @Autowired
    private UsuarioRepository usuarioRepository;

    @PrePersist
    public void handleSaveCliente(final Cliente cliente) {
	AutowireHelper.autowire(this, this.usuarioRepository);
	final Usuario usuario = UsuarioUtils.getUsuarioFromContext(this.usuarioRepository);
	cliente.setIdPeluqueria(usuario.getIdPeluqueria());
    }

    // @PreUpdate no se utiliza por problemas con la transaccionalidad

    @PreRemove
    public void handleRemoveCliente(final Cliente cliente) {
	AutowireHelper.autowire(this, this.usuarioRepository);
	final Usuario usuario = UsuarioUtils.getUsuarioFromContext(this.usuarioRepository);
	if (!cliente.getIdPeluqueria().equals(usuario.getIdPeluqueria())) {
	    throw new AccessDeniedException("The client does not match the current user's idPeluqueria");
	}
    }

}
