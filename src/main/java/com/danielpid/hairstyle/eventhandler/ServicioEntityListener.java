package com.danielpid.hairstyle.eventhandler;

import javax.persistence.PrePersist;
import javax.persistence.PreRemove;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.stereotype.Component;

import com.danielpid.hairstyle.entity.Servicio;
import com.danielpid.hairstyle.entity.Usuario;
import com.danielpid.hairstyle.repository.api.UsuarioRepository;
import com.danielpid.hairstyle.util.UsuarioUtils;

@Component
public class ServicioEntityListener {

    @Autowired
    private UsuarioRepository usuarioRepository;

    @PrePersist
    public void handleSaveServicio(final Servicio servicio) {
	AutowireHelper.autowire(this, this.usuarioRepository);
	final Usuario usuario = UsuarioUtils.getUsuarioFromContext(this.usuarioRepository);
	servicio.setIdPeluqueria(usuario.getIdPeluqueria());
    }

    // @PreUpdate no se utiliza por problemas con la transaccionalidad

    @PreRemove
    public void handleRemoveServicio(final Servicio servicio) {
	AutowireHelper.autowire(this, this.usuarioRepository);
	final Usuario usuario = UsuarioUtils.getUsuarioFromContext(this.usuarioRepository);
	if (!servicio.getIdPeluqueria().equals(usuario.getIdPeluqueria())) {
	    throw new AccessDeniedException("The servicio does not match the current user's idPeluqueria");
	}
    }

}
