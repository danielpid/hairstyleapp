package com.danielpid.hairstyle.eventhandler;

import javax.persistence.PrePersist;
import javax.persistence.PreRemove;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.stereotype.Component;

import com.danielpid.hairstyle.entity.Cita;
import com.danielpid.hairstyle.entity.Usuario;
import com.danielpid.hairstyle.repository.api.UsuarioRepository;
import com.danielpid.hairstyle.util.UsuarioUtils;

@Component("citaEntityListener")
public class CitaEntityListener {

    @Autowired
    private UsuarioRepository usuarioRepository;

    @PrePersist
    public void handleSaveCita(final Cita cita) {
	AutowireHelper.autowire(this, this.usuarioRepository);
	final Usuario usuario = UsuarioUtils.getUsuarioFromContext(this.usuarioRepository);
	if (!cita.getIdPeluqueria().equals(usuario.getIdPeluqueria())) {
	    throw new AccessDeniedException("The appointment does not match the current user's idPeluqueria");
	}
    }

    // @PreUpdate no se utiliza por problemas con la transaccionalidad

    @PreRemove
    public void handleRemoveCita(final Cita cita) {
	AutowireHelper.autowire(this, this.usuarioRepository);
	final Usuario usuario = UsuarioUtils.getUsuarioFromContext(this.usuarioRepository);
	if (!cita.getIdPeluqueria().equals(usuario.getIdPeluqueria())) {
	    throw new AccessDeniedException("The appointment does not match the current user's idPeluqueria");
	}
    }

}
