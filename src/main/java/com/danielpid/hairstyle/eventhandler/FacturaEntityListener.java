package com.danielpid.hairstyle.eventhandler;

import javax.persistence.PrePersist;
import javax.persistence.PreRemove;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.stereotype.Component;

import com.danielpid.hairstyle.entity.Factura;
import com.danielpid.hairstyle.entity.Usuario;
import com.danielpid.hairstyle.repository.api.UsuarioRepository;
import com.danielpid.hairstyle.util.UsuarioUtils;

@Component("facturaEntityListener")
public class FacturaEntityListener {

    @Autowired
    private UsuarioRepository usuarioRepository;

    @PrePersist
    public void handleSaveFactura(final Factura factura) {
	AutowireHelper.autowire(this, this.usuarioRepository);
	final Usuario usuario = UsuarioUtils.getUsuarioFromContext(this.usuarioRepository);
	if (!factura.getIdPeluqueria().equals(usuario.getIdPeluqueria())) {
	    throw new AccessDeniedException("The receipt does not match the current user's idPeluqueria");
	}
    }

    // @PreUpdate no se utiliza por problemas con la transaccionalidad

    @PreRemove
    public void handleRemoveFactura(final Factura factura) {
	AutowireHelper.autowire(this, this.usuarioRepository);
	final Usuario usuario = UsuarioUtils.getUsuarioFromContext(this.usuarioRepository);
	if (!factura.getIdPeluqueria().equals(usuario.getIdPeluqueria())) {
	    throw new AccessDeniedException("The receipt does not match the current user's idPeluqueria");
	}
    }

}
