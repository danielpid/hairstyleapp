package com.danielpid.hairstyle.eventhandler;

import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class AutowireHelper implements ApplicationContextAware {

    private static final AutowireHelper INSTANCE = new AutowireHelper();
    private static ApplicationContext applicationContext;

    /**
     * Tries to autowire the specified instance of the class if one of the specified beans which need to be autowired
     * are null.
     *
     * @param classToAutowire
     *            the instance of the class which holds @Autowire annotations
     * @param beansToAutowireInClass
     *            the beans which have the @Autowire annotation in the specified {#classToAutowire}
     */
    public static void autowire(final Object classToAutowire, final Object... beansToAutowireInClass) {
	for (final Object bean : beansToAutowireInClass) {
	    if (bean == null) {
		applicationContext.getAutowireCapableBeanFactory().autowireBean(classToAutowire);
		return;
	    }
	}
    }

    @Override
    public void setApplicationContext(final ApplicationContext applicationContext) {
	AutowireHelper.applicationContext = applicationContext;
    }

    /**
     * @return the singleton instance.
     */
    @Bean
    public static AutowireHelper getInstance() {
	return INSTANCE;
    }

}