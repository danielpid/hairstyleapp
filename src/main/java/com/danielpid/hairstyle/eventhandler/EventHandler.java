/*
 * Copyright 2015 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.danielpid.hairstyle.eventhandler;

/**
 * @author Greg Turnquist
 */
// tag::code[]
// @Component
// @RepositoryEventHandler(Employee.class)
public class EventHandler {

    // private final SimpMessagingTemplate websocket;

    // private final EntityLinks entityLinks;

    // @Autowired
    // public EventHandler(final SimpMessagingTemplate websocket, final EntityLinks entityLinks) {
    // this.websocket = websocket;
    // this.entityLinks = entityLinks;
    // }

    // @HandleAfterCreate
    // public void newEmployee(final Employee employee) {
    // this.websocket.convertAndSend(MESSAGE_PREFIX + "/newEmployee", getPath(employee));
    // }
    //
    // @HandleAfterDelete
    // public void deleteEmployee(final Employee employee) {
    // this.websocket.convertAndSend(MESSAGE_PREFIX + "/deleteEmployee", getPath(employee));
    // }
    //
    // @HandleAfterSave
    // public void updateEmployee(final Employee employee) {
    // this.websocket.convertAndSend(MESSAGE_PREFIX + "/updateEmployee", getPath(employee));
    // }
    //
    // /**
    // * Take an {@link Employee} and get the URI using Spring Data REST's {@link EntityLinks}.
    // *
    // * @param employee
    // */
    // private String getPath(final Employee employee) {
    // return this.entityLinks.linkForSingleResource(employee.getClass(), employee.getId()).toUri().getPath();
    // }

}
// end::code[]
